using System;
using Xunit;
using sadaAG;
using System.Collections.Generic;

namespace sadaAGTest
{
    public class IndividuoFactoryTest
    {
        [Fact]
        public void CrearIndividuoRandomTest()
        {
            IndividuoFactory fac = new IndividuoFactory();

            List<Aula> aulas = new List<Aula>(5);
            aulas.Add(new Aula(1));
            aulas.Add(new Aula(2));
            aulas.Add(new Aula(3));
            aulas.Add(new Aula(4));
            aulas.Add(new Aula(5));

            List<Comision> comisiones = new List<Comision>(10);
            comisiones.Add(new Comision(1, 4));
            comisiones.Add(new Comision(2, 2));
            comisiones.Add(new Comision(3, 6));
            comisiones.Add(new Comision(4, 3));
            comisiones.Add(new Comision(5, 2));
            comisiones.Add(new Comision(6, 4));
            comisiones.Add(new Comision(7, 4));
            comisiones.Add(new Comision(8, 6));
            comisiones.Add(new Comision(9, 4));
            comisiones.Add(new Comision(10, 2));

            Individuo individuo = null; // fac.CrearIndividuoRandom(aulas, comisiones);

            Assert.NotNull(individuo);
            Assert.Equal(5, individuo.Cromosoma.Count);
            Assert.Equal(8, ((GenCompuesto)individuo.Cromosoma[0]).Valor.Length);

        }

        [Fact]
        public void Crear1000IndividuosRandomTest()
        {

            IndividuoFactory fac = new IndividuoFactory();

            List<Aula> aulas = new List<Aula>();
            aulas.Add(new Aula(1));
            aulas.Add(new Aula(2));
            aulas.Add(new Aula(3));
            aulas.Add(new Aula(4));
            aulas.Add(new Aula(5));
            aulas.Add(new Aula(6));
            aulas.Add(new Aula(7));
            aulas.Add(new Aula(8));
            aulas.Add(new Aula(9));
            aulas.Add(new Aula(10));

            List<Comision> comisiones = new List<Comision>(30);
            comisiones.Add(new Comision(1, 4));
            comisiones.Add(new Comision(2, 8));
            comisiones.Add(new Comision(3, 6));
            comisiones.Add(new Comision(4, 6));
            comisiones.Add(new Comision(5, 4));
            comisiones.Add(new Comision(6, 4));
            comisiones.Add(new Comision(7, 4));
            comisiones.Add(new Comision(8, 6));
            comisiones.Add(new Comision(9, 8));
            comisiones.Add(new Comision(10, 4));
            comisiones.Add(new Comision(11, 8));
            comisiones.Add(new Comision(12, 4));
            comisiones.Add(new Comision(13, 6));
            comisiones.Add(new Comision(14, 6));
            comisiones.Add(new Comision(15, 4));
            comisiones.Add(new Comision(16, 8));
            comisiones.Add(new Comision(17, 4));
            comisiones.Add(new Comision(18, 6));
            comisiones.Add(new Comision(19, 4));
            comisiones.Add(new Comision(20, 2));
            comisiones.Add(new Comision(21, 4));
            comisiones.Add(new Comision(22, 2));
            comisiones.Add(new Comision(23, 6));
            comisiones.Add(new Comision(24, 3));
            comisiones.Add(new Comision(25, 2));
            comisiones.Add(new Comision(26, 4));
            comisiones.Add(new Comision(27, 4));
            comisiones.Add(new Comision(28, 6));
            comisiones.Add(new Comision(29, 4));
            comisiones.Add(new Comision(30, 2));
            comisiones.Add(new Comision(31, 4));
            comisiones.Add(new Comision(32, 8));
            comisiones.Add(new Comision(33, 6));
            comisiones.Add(new Comision(34, 6));
            comisiones.Add(new Comision(35, 4));
            comisiones.Add(new Comision(36, 4));
            comisiones.Add(new Comision(37, 4));
            comisiones.Add(new Comision(38, 6));
            comisiones.Add(new Comision(39, 8));
            comisiones.Add(new Comision(40, 4));
            comisiones.Add(new Comision(41, 8));
            comisiones.Add(new Comision(42, 4));
            comisiones.Add(new Comision(43, 6));
            comisiones.Add(new Comision(44, 6));
            comisiones.Add(new Comision(45, 4));
            comisiones.Add(new Comision(46, 8));
            comisiones.Add(new Comision(47, 4));
            comisiones.Add(new Comision(48, 6));
            comisiones.Add(new Comision(49, 4));
            comisiones.Add(new Comision(50, 2));
            comisiones.Add(new Comision(51, 4));
            comisiones.Add(new Comision(52, 2));
            comisiones.Add(new Comision(53, 6));
            comisiones.Add(new Comision(54, 3));
            comisiones.Add(new Comision(55, 2));
            comisiones.Add(new Comision(56, 4));
            comisiones.Add(new Comision(57, 4));
            comisiones.Add(new Comision(58, 6));
            comisiones.Add(new Comision(59, 4));
            comisiones.Add(new Comision(60, 2));

            //Individuo individuo = fac.CrearIndividuoRandom(aulas, comisiones, 8);

            //Console.WriteLine("Individuo creado con �xito!");

            List<Individuo> poblacion = new List<Individuo>();
            for (int i = 0; i < 1000; i++)
            {
                /*Console.WriteLine("Individuo: " + i);
                Console.WriteLine(fac.CrearIndividuoRandom(aulas, comisiones, 45));
                Console.WriteLine();*/

                //poblacion.Add(fac.CrearIndividuoRandom(aulas, comisiones, 45));
            }

            Assert.Equal(1000, poblacion.Count);

        }
    }
}
