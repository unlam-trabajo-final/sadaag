﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using modelo;
using sadaAG;

namespace ConsoleAG
{
    class Program
    {
        static Dictionary<string, string> argumentos = new Dictionary<string, string>();

        static readonly string Titulo = "---------------------------------------------\n" +
                                        "SADA - Sistema de Asignación Digital de Aulas\n" +
                                        "---------------------------------------------\n" +
                                        "Grupo 107 - Trabajo Final - UNLaM - 2020\n";
        static void Main(string[] args)
        {

            Console.WriteLine(Titulo);
            if (args.Contains<string>("help"))
            {
                Console.WriteLine("Parámetros por defecto:");
                Console.WriteLine("\tmutacion=1 (porcentaje, valor entero)");
                Console.WriteLine("\tpoblacion=15000 (valor entero)");
                Console.WriteLine("\tgeneraciones=100 (valor entero)");
                Console.WriteLine("\telitismo=false (true / false)");
                Console.WriteLine("\thilos=0 (0: Single thread; 1..n: Multi thread)");
                Console.WriteLine("\tsalida=PANTALLA (PANTALLA / CSV)");
                Console.WriteLine("\tarchivo=dataset_10com_12hs.json (nombre de archivo)");
                Console.WriteLine("\tdistribucion=AUTOMATICA (AUTOMATICA / FRAGMENTADA / SECUENCIAL)");
                Console.WriteLine("Escriba los parámetros en la línea de comandos, deparados por espacios y respetando el formato de arriba.");

                return;
            }

            // Carga los argumentos en un diccionario de datos
            argumentos = cargarArgumentos(args);

            // Cargar el json
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Archivos\" + argumentos["archivo"]);
            string jsonString = File.ReadAllText(path);

            // Crear el Dataset
            DatasetList datasetList = JsonSerializer.Deserialize<DatasetList>(jsonString);
            // Cargar los objetos a partir de sus Ids
            datasetList.CargarObjetos();

            // Convierte a un Dataset con diccionarios
            Dataset dataset = datasetList.toDataset();

            // Imprime la configuración
            Console.WriteLine("Configuración: ");
            foreach (KeyValuePair<string, string> elemento in argumentos)
                Console.Write(" {0}={1}", elemento.Key, elemento.Value);
            Console.WriteLine("\n");

            // Verifica la relación entre la oferta de horas y la demanda
            int horasDemandadas = 0;
            datasetList.Comisiones.ForEach(c => horasDemandadas += c.Horas);
            int ofertaHoraria = datasetList.CantidadHorasSemanales * datasetList.Aulas.Count;
            int horasLibres = ofertaHoraria - horasDemandadas;

            // Determina el tipo de Fábrica de Individuos según las horas libres
            IIndividuoFactory individuoFactory = null;
            string mensajeFactory = "";

            if (horasLibres > datasetList.Comisiones.Count * 4)
            {
                individuoFactory = new IndividuoFactoryDistribucionHomogenea();
                mensajeFactory = string.Format("La oferta horaria ({0}hs) es ampliamente mayor que la demanda ({1}hs). Distribución inicial: FRAGMENTADA.", ofertaHoraria, horasDemandadas);
            }
            else {
                individuoFactory = new IndividuoFactory();
                //if (horasLibres < datasetList.Comisiones.Count)
                if (ofertaHoraria < horasDemandadas)
                    mensajeFactory = string.Format("La oferta horaria ({0}hs) es menor que la demanda ({1}hs). Se minimizará el error.", ofertaHoraria, horasDemandadas);
                else
                    mensajeFactory = string.Format("La oferta horaria ({0}hs) y la demanda ({1}hs) son del mismo orden.", ofertaHoraria, horasDemandadas);

                mensajeFactory += " Distribución inical: SIN FRAGMENTAR.";
            }

            Console.WriteLine("NOTA: {0}\n", mensajeFactory);
            Console.WriteLine("Presione una tecla para comenzar...");
            Console.ReadLine();

            // Crear el AlgoritmoGenetico y ejecutarlo
            AlgoritmoGeneticoBuilder builder = new AlgoritmoGeneticoBuilder()
                                        .SetCrossover(new Crossover())
                                        .SetFuncionFitness(new FuncionFitness(dataset))
                                        .SetMutacion(new Mutacion { Dataset = datasetList, Porcentaje = int.Parse(argumentos["mutacion"]) })
                                        .SetSeleccion(new Seleccion())
                                        .SetDataset(dataset)
                                        .SetDatasetList(datasetList)
                                        .SetCantidadIndividuos(int.Parse(argumentos["poblacion"]))
                                        .SetCantidadGeneraciones(int.Parse(argumentos["generaciones"]))
                                        .SetElitismo(bool.Parse(argumentos["elitismo"]))
                                        .SetIndividuoFactory(individuoFactory);

            // Si hay mas de 0 Hilos, se realiza una ejecución en paralelo
            int hilos = int.Parse(argumentos["hilos"]);
            if (hilos <= 0)
            {
                IAlgoritmoGenetico ag = builder.CrearAlgoritmoGenetico();
                ag.OnEvolucionProgreso += Ag_OnEvolucionProgreso;
                ag.OnEvolucionFin += Ag_OnEvolucionFin;
                ag.Run();
            }
            else
            {

                List<IAlgoritmoGenetico> listaAG = builder.CrearAlgoritmosParalelos(hilos);
                Task[] tareas = new Task[listaAG.Count];

                Parallel.ForEach(listaAG, (a) =>
                {
                    a.OnEvolucionProgreso += Ag_OnEvolucionProgreso;

                    a.Run();

                });
                
            }
        }

        private static void Ag_OnEvolucionFin(object sender, EvolucionFinEventArgs e)
        {
            //Console.WriteLine("{0}  ", e.Datos.ToJSON());
        }

        private static void Ag_OnEvolucionProgreso(object sender, EvolucionProgresoEventArgs e)
        {
            if (argumentos["salida"].Equals("PANTALLA"))
                Console.WriteLine("{0}  ", e.Datos);
            else
                Console.WriteLine("{0}", e.Datos.ToCSV());
        }

        private static void EjecutarHilos(AlgoritmoGeneticoBuilder builder, int hilos)
        {
            List<IAlgoritmoGenetico> listaAG = builder.CrearAlgoritmosParalelos(hilos);

            foreach (IAlgoritmoGenetico a in listaAG)
            {
                a.OnEvolucionProgreso += Ag_OnEvolucionProgreso;

                Task.Run(() => a.Run());
            }
        }

        private static Dictionary<string, string> cargarArgumentos(string[] args)
        {
            Dictionary<string, string> argumentos = new Dictionary<string, string>()
            {
                { "mutacion", "1" },
                { "poblacion", "15000" },
                { "generaciones", "100" },
                { "elitismo", "false" },
                { "hilos", "0" },
                { "salida", "PANTALLA" },
                { "archivo", "dataset_10com_12hs.json" },
                { "distribucion", "AUTOMATICA" }
            };

            // Analiza y extrae los datos desde la línea de comandos
            foreach (string argumento in args)
            {
                //Console.WriteLine(argumento);
                if (argumento.Contains('='))
                {
                    string[] par = argumento.Split('=', StringSplitOptions.RemoveEmptyEntries);
                    if (argumento.Contains(par[0]))
                        argumentos[par[0]] = par[1];
                    else
                        argumentos.Add(par[0], par[1]);
                }
            }
            return argumentos;
        }
    }
}