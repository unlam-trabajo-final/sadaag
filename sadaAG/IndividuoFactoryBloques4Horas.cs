﻿using modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    public class IndividuoFactoryBloques4Horas : IIndividuoFactory
    {
        private readonly Random rnd = new Random();
        /// <summary>
        /// Crea una solución entera, al azar. Entre sus parámetros recibe una lista de <c>Comisiones</c>. Toma cada comisión y asiga todas
        /// sus horas de manera secuencial. Para asegurar la aleatoriedad, para cada cromosoma genera una lista distinta de índices para acceder a las aulas 
        /// en un orden distinto cada vez.
        /// </summary>
        /// <returns>Devuelve un nuevo <c>Individuo</c>.</returns>
        public Individuo CrearIndividuoRandom(DatasetList dataset)
        {
            List<IGen> cromosoma = new List<IGen>(dataset.Aulas.Count);

            // Limpia las horas asignadas de las Comisiones
            dataset.Comisiones.ForEach(com => com.HorasAsignadas = 0);

            // Generar un array con el orden de acceso aleatoreo a las aulas
            int[] indicesAulas = ArrayExtensions.CrearArrayEnteros(dataset.Aulas.Count, 0);
            rnd.Shuffle<int>(indicesAulas);

            // Crear cromosoma con genes vacios (List de horarios para cada aula).
            for (int i = 0; i < dataset.Aulas.Count; i++)
                cromosoma.Add(null);

            // Crea una copia de la lista de Comisiones
            List<Comision> listaComsiones = new List<Comision>(dataset.Comisiones);

            // Generar la cantidad de Genes del cromosoma (Cada cromosoma es un aula con sus asignaciones).
            foreach (int indice in indicesAulas)
                cromosoma[indice] = CrearGenRandom(dataset.CantidadHorasSemanales, listaComsiones);

            Individuo individuo = new Individuo
            {
                Cromosoma = cromosoma
            };

            return individuo;
        }

        /// <summary>
        /// Recibe una cantidad de horas y una pila de comisiones. Va colocando secuencialmente en cada elemento de la lista resultante, el Id de la comisión. El Id de la misma 
        /// comisión se repite por cada hora que necesita asignar esa Comisión.
        /// En el caso de acabarse el lugar en un Aula, se completa en otro Aula.
        /// </summary>
        /// <param name="cantidadHoras">Representa la cantidad de horas semanales que brinda cada <c>Aula</c>.</param>
        /// <param name="pilaComisiones">Es una pila que contiene las comisiones desordenadas a propósito.</param>
        /// <returns>Devuelve un objeto <c>IGen</c> que contiene una lista de valores enteros. Este Gen representa todo el horario de un <c>Aula</c>.</returns>
        private IGen CrearGenRandom(int cantidadHoras, List<Comision> listaComisiones)
        {
            //GenCompuesto gen = new GenCompuesto(new List<IGen>());
            GenCompuesto gen = new GenCompuesto(new int[cantidadHoras]);

            Comision comision = null;
            int comisionId = 0;

            if (listaComisiones.Count > 0)
            {
                comision = listaComisiones[rnd.Next(listaComisiones.Count)];
                comisionId = comision.Id;
            }
            // Generar la cantidad de genes internos
            for (int i = 0, horasSeguidas = 0; i < cantidadHoras; i++, horasSeguidas++) /// Acá se puede cortar el ciclo a las 4 horas seguidas de la comisión
            {
                // Asignar Comisión
                //gen.Valor.Add(new Gen(comisionId));
                gen.Valor[i] = comisionId;

                if (comision != null && comisionId != 0)
                {
                    comision.HorasAsignadas++;
                    horasSeguidas++;

                    // Si ya se asignaron todas las horas, se elimina esa comisión y se lee otra.
                    if (comision.HorasAsignadas == comision.Horas || horasSeguidas==5)
                    {
                        comisionId = 0;
                        horasSeguidas = 0;

                        if (comision.HorasAsignadas == comision.Horas)
                            listaComisiones.Remove(comision);

                        if (listaComisiones.Count > 0)
                        {
                            comision = listaComisiones[rnd.Next(listaComisiones.Count)];
                            comisionId = comision.Id;
                        }
                    }
                }
            }
            return gen;
        }
    }
}
