﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sadaAG
{
    public class Crossover : ICrossover
    {
        private readonly Random random = new Random();

        /*public Individuo[] Cruzar(Individuo padreOriginal, Individuo madreOriginal)
        {
            // Creamos copias de los padres originales para poder manipular y tomar referencias de sus Cromosomas
            Individuo padre = padreOriginal.Clonar();
            Individuo madre = madreOriginal.Clonar();

            // Selecciona los puntos de división
            int puntoCromosoma;
            GenCompuesto genSeleccionadoPadre;
            GenCompuesto genSeleccionadoMadre;

            puntoCromosoma = random.Next(0, padre.Cromosoma.Count);
            genSeleccionadoPadre = (GenCompuesto)padre.Cromosoma[puntoCromosoma];
            genSeleccionadoMadre = (GenCompuesto)madre.Cromosoma[puntoCromosoma];

            //int puntoGen = random.Next(0, genSeleccionadoPadre.Valor.Count);
            int puntoGen = random.Next(0, genSeleccionadoPadre.Valor.Length);

            // Se cruzan los cromosomas padres para obtener la combinación para crear los hijos.
            List<IGen>[] cromosomasNuevos = CruzarListas<IGen>(padre.Cromosoma,madre.Cromosoma, puntoCromosoma);

            // Se combinan los GenesCompuestos que fueron seleccionados como punto de cruce.
            List<IGen>[] segmentosPadre = DividirListaUnPunto<IGen>(genSeleccionadoPadre.Valor, puntoGen);
            List<IGen>[] segmentosMadre = DividirListaUnPunto<IGen>(genSeleccionadoMadre.Valor, puntoGen);
            
            // Se inyectan los genes combinados a cromosomas de los descendientes
            List<IGen> nuevoGen1 = segmentosPadre[0]; nuevoGen1.AddRange(segmentosMadre[1]);
            List<IGen> nuevoGen2 = segmentosMadre[0]; nuevoGen2.AddRange(segmentosPadre[1]);
            ((GenCompuesto)cromosomasNuevos[0][puntoCromosoma]).Valor = nuevoGen1;
            ((GenCompuesto)cromosomasNuevos[1][puntoCromosoma]).Valor = nuevoGen2;
            
            // Asigna la genética combinada a lso dos descendientes.
            Individuo hijo1 = new Individuo();
            Individuo hijo2 = new Individuo();
            hijo1.Cromosoma = cromosomasNuevos[0];
            hijo2.Cromosoma = cromosomasNuevos[1];

            return new Individuo[] { hijo1, hijo2 };
        }*/

        public Individuo[] Cruzar(Individuo padreOriginal, Individuo madreOriginal)
        {
            // Creamos copias de los padres originales para poder manipular y tomar referencias de sus Cromosomas
            Individuo padre = padreOriginal.Clonar();
            Individuo madre = madreOriginal.Clonar();

            // Selecciona los puntos de división
            int puntoCromosoma;
            GenCompuesto genSeleccionadoPadre;
            GenCompuesto genSeleccionadoMadre;

            puntoCromosoma = random.Next(0, padre.Cromosoma.Count);
            genSeleccionadoPadre = (GenCompuesto)padre.Cromosoma[puntoCromosoma];
            genSeleccionadoMadre = (GenCompuesto)madre.Cromosoma[puntoCromosoma];

            //int puntoGen = random.Next(0, genSeleccionadoPadre.Valor.Count);
            int largoGen = genSeleccionadoPadre.Valor.Length;
            int puntoGen = random.Next(0, largoGen);

            // Se cruzan los cromosomas padres para obtener la combinación para crear los hijos.
            List<IGen>[] cromosomasNuevos = CruzarListas<IGen>(padre.Cromosoma, madre.Cromosoma, puntoCromosoma);

            // Se combinan los GenesCompuestos que fueron seleccionados como punto de cruce.
            ArrayExtensions.Split<int>(genSeleccionadoPadre.Valor, puntoGen, out int[] segmentoPadre1, out int[] segmentoPadre2);
            ArrayExtensions.Split<int>(genSeleccionadoMadre.Valor, puntoGen, out int[] segmentoMadre1, out int[] segmentoMadre2);

            // Se inyectan los genes combinados a cromosomas de los descendientes
            int[] nuevoGen1 = new int[largoGen];
            Array.Copy(genSeleccionadoPadre.Valor,           0, nuevoGen1,          0,  puntoGen+1);
            Array.Copy(genSeleccionadoMadre.Valor,  puntoGen+1, nuevoGen1, puntoGen+1,  largoGen - (puntoGen+1));

            int[] nuevoGen2 = new int[largoGen];
            Array.Copy(genSeleccionadoMadre.Valor, 0, nuevoGen2, 0, puntoGen + 1);
            Array.Copy(genSeleccionadoPadre.Valor, puntoGen + 1, nuevoGen2, puntoGen + 1, largoGen - (puntoGen + 1));

            ((GenCompuesto)cromosomasNuevos[0][puntoCromosoma]).Valor = nuevoGen1;
            ((GenCompuesto)cromosomasNuevos[1][puntoCromosoma]).Valor = nuevoGen2;

            // Asigna la genética combinada a lso dos descendientes.
            Individuo hijo1 = new Individuo();
            Individuo hijo2 = new Individuo();
            hijo1.Cromosoma = cromosomasNuevos[0];
            hijo2.Cromosoma = cromosomasNuevos[1];

            return new Individuo[] { hijo1, hijo2 };
        }

        private List<T>[] CruzarListas<T>(List<T> padre, List<T> madre, int punto)
        {
            List<T>[] segmentosPadre = DividirListaUnPunto<T>(padre, punto);
            List<T>[] segmentosMadre = DividirListaUnPunto<T>(madre, punto);

            List<T> hijo1 = segmentosPadre[0]; hijo1.AddRange(segmentosMadre[1]);
            List<T> hijo2 = segmentosMadre[0]; hijo2.AddRange(segmentosPadre[1]);

            return new List<T>[] { hijo1, hijo2 };
        }

        private List<T>[] DividirListaUnPunto<T>(List<T> lista, int punto)
        {
            List<T> segmento1, segmento2;

            // El segmento1 se queda con el punto de división
            segmento1 = new List<T>(lista.GetRange(0, punto + 1));

            if (punto + 1 < lista.Count)
                segmento2 = new List<T>(lista.GetRange(punto + 1, lista.Count - (punto + 1)));
            else
                segmento2 = new List<T>();

            return new List<T>[] { segmento1, segmento2 };
        }
    }
}
