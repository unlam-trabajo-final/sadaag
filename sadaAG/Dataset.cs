﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using sadaAG;

namespace modelo
{
    /// <summary>
    /// Esta clase agrupa toda la representación en memoria de los datos de entrada. Debe ser poblada por objetos obtenidos de alguna fuente externa (preferiblemente un archivo json).
    /// </summary>
    public class Dataset 
    {
        public Dictionary<int, Aula> Aulas { get; set; } = new Dictionary<int, Aula>();
        public Dictionary<int, Comision> Comisiones { get; set; } = new Dictionary<int, Comision>();
        public Dictionary<int, Recurso> Prestaciones { get; set; } = new Dictionary<int, Recurso>();
        
        public int CantidadHorasSemanales { get; set; }

    }
}
