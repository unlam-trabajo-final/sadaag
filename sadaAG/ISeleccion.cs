﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// Siguiendo el patrón de diseño Strategy, <c>ISeleccion</c> representa la interfaz de los objetos que contengan distintos algoritmos de selección.
    /// </summary>
    public interface ISeleccion
    {
        /// <summary>
        /// Algoritmo que seleciona un individuo para la reproducción.
        /// </summary>
        /// <param name="poblacion">Población actual de objetos <c>Individuo</c>.</param>
        /// <returns>Devuelve el <c>Individuo</c> seleccionado.</returns>
        //Individuo Seleccionar(List<Individuo> poblacion, out Individuo perdedor, double porcentajeElitismo, bool eliminarSeleccionado);
        Individuo Seleccionar(List<Individuo> poblacion, bool eliminarSeleccionado);

        Individuo Seleccionar(List<Individuo> poblacion, out Individuo perdedor, bool eliminarSeleccionado);
    }

}
