﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// Superclase abstracta que contiene el comportamiento para cargar las <c>Prestaciones</c> a partir de un array de enteros,
    /// <c>IdsPrestaciones</c>. De esta clase derivan <c>Aula</c> y <c>Comision</c>.
    /// </summary>
    public abstract class EntidadConPrestaciones
    {
        /// <summary>
        /// Contiene la lista de Ids de Prestaciones proveniente del archivo JSON.
        /// </summary>
        public List<int> IdsPrestaciones { get; set; }

        /// <summary>
        /// Contiene las prestaciones en forma de lista de ojetos <c>IPrestacion</c>.
        /// </summary>
        public List<Recurso> Prestaciones { get; set; } = new List<Recurso>();

        /// <summary>
        /// Carga en su propia lista las referencias del diccionario de prestaciones, basándose en la lista de Ids.
        /// </summary>
        /// <param name="prestaciones">Diccionario de objetos <c>IPrestacion</c>.</param>
        public void CargarPrestaciones(List<Recurso> fuentePrestaciones)
        {
            Recurso prestacion;
            foreach (int id in IdsPrestaciones)
            {
                prestacion = fuentePrestaciones.Find(x => x.Id == id);
                if (prestacion != null)
                    Prestaciones.Add(prestacion);
            }
        }
    }
}
