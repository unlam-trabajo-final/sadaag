﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// La clase <c>Prestacion</c> representa las prestaciones que poseen las aulas y requieren las comisiones.
    /// </summary>
    public class Recurso 
    {
        /// <summary>
        /// Identificador único de la prestación.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre de la prestación.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Peso de la prestación. Este peso representa la importancia que tiene en relación a otras prestaciones. Un dato crucial a la hora de decidir brindar o no una prestación.
        /// </summary>
        public int Peso { get; set; }

        public Recurso()
        {
        }
        public Recurso(int id, string nombre, int peso)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.Peso = peso;
        }
    }
}
