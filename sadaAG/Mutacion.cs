﻿using modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    public class Mutacion : IMutacion
    {
        public int Porcentaje { get; set; }
        public DatasetList Dataset { get; set; }

        private readonly Random random = new Random();

        public void Mutar(Individuo individuo)
        {
            // 0 es el valor a ser elejido entre el total
            if (random.Next(100/Porcentaje) == 0)
            {
                GenCompuesto genCompuesto = (GenCompuesto)individuo.Cromosoma[random.Next(individuo.Cromosoma.Count)];
                int indice = random.Next(-1, Dataset.Comisiones.Count);
                int valor = indice > -1 ? Dataset.Comisiones[indice].Id : 0;
                //genCompuesto.Valor[random.Next(genCompuesto.Valor.Count)] = new Gen(valor);
                genCompuesto.Valor[random.Next(genCompuesto.Valor.Length)] = valor;
            }
        }
    }
}
