﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Channels;
using modelo;

namespace sadaAG
{
    /// <summary>
    /// Utilizamos el Patrón <c>Builder</c> para construir de manera flexible la instancia de <c>AlgoritmoGenetico</c>.
    /// </summary>
    public class AlgoritmoGeneticoBuilder
    {
        public IAlgoritmoGenetico AG = new AlgoritmoGenetico();
        public List<IAlgoritmoGenetico> pool;

        public IAlgoritmoGenetico CrearAlgoritmoGenetico()
        {
            return AG;
        }
        public List<IAlgoritmoGenetico> CrearAlgoritmosParalelos(int instancias)
        {
            Channel<bool> canal = Channel.CreateUnbounded<bool>();
            pool = new List<IAlgoritmoGenetico>();
            for (int i=0; i<instancias; i++)
            {
                pool.Add(new AlgoritmoGeneticoParalelo()
                {
                    Nombre = string.Format("AG{0,3:D3}" , i),
                    Crossover = AG.Crossover,
                    FuncionFitness = AG.FuncionFitness,
                    Mutacion = AG.Mutacion,
                    Seleccion = AG.Seleccion,
                    Dataset = AG.Dataset,
                    DatasetList = AG.DatasetList,
                    CantidadGeneraciones = AG.CantidadGeneraciones,
                    CantidadIndividuos = AG.CantidadIndividuos,
                    Elitismo = AG.Elitismo,
                    Canal = canal,
                    IndividuoFactory = AG.IndividuoFactory
                });
            }
            return pool;
        }

        public AlgoritmoGeneticoBuilder SetCrossover(ICrossover crossover)
        {
            AG.Crossover = crossover;
            return this;
        }

        public AlgoritmoGeneticoBuilder SetFuncionFitness(IFuncionFitness funcionFitness)
        {
            AG.FuncionFitness = funcionFitness;
            return this;
        }

        public AlgoritmoGeneticoBuilder SetMutacion(IMutacion mutacion)
        {
            AG.Mutacion = mutacion;
            return this;
        }

        public AlgoritmoGeneticoBuilder SetSeleccion(ISeleccion seleccion)
        {
            AG.Seleccion = seleccion;
            return this;
        }
        public AlgoritmoGeneticoBuilder SetDataset(Dataset dataset)
        {
            AG.Dataset = dataset;
            return this;
        }
        public AlgoritmoGeneticoBuilder SetDatasetList(DatasetList datasetList)
        {
            AG.DatasetList = datasetList;
            return this;
        }
        public AlgoritmoGeneticoBuilder SetCantidadGeneraciones(int cantidad)
        {
            AG.CantidadGeneraciones = cantidad;
            return this;
        }
        public AlgoritmoGeneticoBuilder SetCantidadIndividuos(int cantidad)
        {
            AG.CantidadIndividuos = cantidad;
            return this;
        }
        public AlgoritmoGeneticoBuilder SetElitismo(bool elitismo)
        {
            AG.Elitismo = elitismo;
            return this;
        }
        public AlgoritmoGeneticoBuilder SetIndividuoFactory(IIndividuoFactory individuoFactory) 
        {
            AG.IndividuoFactory = individuoFactory;
            return this;
        }
    }
}
