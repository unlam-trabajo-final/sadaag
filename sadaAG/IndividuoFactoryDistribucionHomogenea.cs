﻿using modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// <c>Crea individuos y sus cromosomas son generados de forma aleatoria. La característica de esta fábrica es distribuir los bloques de horas de comisiones de manera homogénea.
    /// Es necesario para darle una buena diversidad genética a la población de  individuos, sobre todo en los casos que la oferta horaria es muy superior a la demanda.
    /// </summary>
    public class IndividuoFactoryDistribucionHomogenea : IIndividuoFactory
    {
        private readonly Random rnd = new Random();
        /// <summary>
        /// Crea una solución entera, al azar. Entre sus parámetros recibe una lista de <c>Comisiones</c>. Toma cada comisión y asiga todas
        /// sus horas de manera secuencial. Para asegurar la aleatoriedad, para cada cromosoma genera una lista distinta de índices para acceder a las aulas 
        /// en un orden distinto cada vez.
        /// </summary>
        /// <returns>Devuelve un nuevo <c>Individuo</c>.</returns>
        public Individuo CrearIndividuoRandom(DatasetList dataset)
        {
            // Limpia las horas asignadas de las Comisiones
            dataset.Comisiones.ForEach(com => com.HorasAsignadas = 0);

            // Genera un array con el orden de acceso aleatorio a las comisiones
            int[] indicesComisiones = ArrayExtensions.CrearArrayEnteros(dataset.Comisiones.Count, 0);
            rnd.Shuffle<int>(indicesComisiones);

            // Generar una pila con las comisiones mezcladas al azar
            Stack<Comision> pilaComisiones = new Stack<Comision>();
            foreach (int indice in indicesComisiones)
                pilaComisiones.Push(dataset.Comisiones[indice]);

            List<IGen> cromosoma = CrearCromosoma(dataset, pilaComisiones);

            Individuo individuo = new Individuo
            {
                Cromosoma = cromosoma
            };
            /*Console.WriteLine(individuo.ToString());
            Console.WriteLine("Presione una tecla para continuar...");
            Console.ReadLine();*/

            return individuo;
        }

        private List<IGen> CrearCromosoma(DatasetList dataset, Stack<Comision> pilaComisiones)
        {
            List<IGen> cromosoma = new List<IGen>(dataset.Aulas.Count);

            // Calcula el total de horas de clase
            int horasClases = 0;
            dataset.Comisiones.ForEach(c => horasClases += c.Horas);

            // Calcula los valores principales
            int horasDisponibles = dataset.Aulas.Count * dataset.CantidadHorasSemanales;
            int horasLibres = horasDisponibles - horasClases;
            int separadores = dataset.Comisiones.Count;
            int largoSeparador = horasLibres / separadores;

            // Representa todo el cromosoma en un array unidimensional
            int[] unidimensional = new int[horasDisponibles];

            // Asignamos secuencialmente las horas de cada comision, intercalando los separadores
            bool okSeparador = rnd.Next(2)==1;
            for (int i=0; i < unidimensional.Length && pilaComisiones.Count > 0;)
            {
                // Se pone el separador al principio si salió así sorteado
                if (okSeparador)
                    i += largoSeparador;
                else
                    okSeparador = true;

                // Tomar una comisión
                Comision comision = pilaComisiones.Pop();

                // Iterar hasta comsumir todas las horas de la comisión
                for (int h = 0; h < comision.Horas && i < unidimensional.Length; h++, comision.HorasAsignadas++, i++)
                    unidimensional[i] = comision.Id;
            }

            //Console.WriteLine(string.Join(",", unidimensional));

            // Convertimos a Genes compuestos
            int indiceHora = 0;
            GenCompuesto genCompuesto = new GenCompuesto(new int[dataset.CantidadHorasSemanales]);
            foreach (int comisionId in unidimensional)
            {
                genCompuesto.Valor[indiceHora] = comisionId;
                indiceHora++;

                // Cuando completa el renglón de un Aula, hace un corte de control
                if (indiceHora == dataset.CantidadHorasSemanales)
                {
                    // Agregamos el Gen compuesto al cromosoma
                    cromosoma.Add(genCompuesto);

                    // Reiniciamos el índice de horas y el gen compuesto
                    indiceHora = 0;
                    genCompuesto = new GenCompuesto(new int[dataset.CantidadHorasSemanales]);
                }
            }

            return cromosoma;
        }


        /// <summary>
        /// Recibe una cantidad de horas y una pila de comisiones. Va colocando secuencialmente en cada elemento de la lista resultante, el Id de la comisión. El Id de la misma 
        /// comisión se repite por cada hora que necesita asignar esa Comisión.
        /// En el caso de acabarse el lugar en un Aula, se completa en otro Aula.
        /// </summary>
        /// <param name="cantidadHoras">Representa la cantidad de horas semanales que brinda cada <c>Aula</c>.</param>
        /// <param name="pilaComisiones">Es una pila que contiene las comisiones desordenadas a propósito.</param>
        /// <returns>Devuelve un objeto <c>IGen</c> que contiene una lista de valores enteros. Este Gen representa todo el horario de un <c>Aula</c>.</returns>
        private IGen CrearGenRandom(int cantidadHoras, Stack<Comision> pilaComisiones)
        {
            GenCompuesto gen = new GenCompuesto(new int[cantidadHoras]);

            Comision comision = null;
            int comisionId = 0;

            if (pilaComisiones.Count > 0)
            {
                comision = pilaComisiones.Peek();
                comisionId = comision.Id;
            }
            // Generar la cantidad de genes internos
            for (int i = 0; i < cantidadHoras; i++) /// Acá se puede cortar el ciclo a las 4 horas seguidas de la comisión
            {
                // Asignar Comisión
                gen.Valor[i] = comisionId;

                if (comision != null && comisionId != 0)
                {
                    comision.HorasAsignadas++;

                    // Si ya se asignaron todas las horas, se elimina esa comisión y se lee otra.
                    if (comision.HorasAsignadas == comision.Horas)
                    {
                        comisionId = 0;

                        if (pilaComisiones.Count > 0)
                            pilaComisiones.Pop();

                        if (pilaComisiones.Count > 0)
                        {
                            comision = pilaComisiones.Peek();
                            comisionId = comision.Id;
                        }
                    }
                }
            }
            return gen;
        }
    }
}
