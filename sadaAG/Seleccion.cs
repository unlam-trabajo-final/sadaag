﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// Este objeto implementa el algritmo de selección por <c>Torneo</c>. Se seleccionan dos individuos al azar, se los evalúa y queda el que mejor <c>Fitness</c> tiene.
    /// </summary>
    public class Seleccion : ISeleccion
    {
        private readonly Random random = new Random();

        public Individuo Seleccionar(List<Individuo> poblacion, bool eliminarSeleccionados)
        {
            return Seleccionar(poblacion, out _, eliminarSeleccionados);
        }

        /// <summary>
        /// Selecciona dos individuos al azar y por torneo queda uno y el otro es eliminado de la población.
        /// </summary>
        /// <param name="poblacion">Población actual.</param>
        /// <returns>Devuelve el individuo seleccionado.</returns>
        //public Individuo Seleccionar(List<Individuo> poblacion, out Individuo perdedor, double porcentajeElitismo, bool eliminarSeleccionados)
        public Individuo Seleccionar(List<Individuo> poblacion, out Individuo perdedor, bool eliminarSeleccionados)
        {
            Individuo seleccionado;
            //Individuo perdedor;

            // Calcula sobre cuantos se hará la selección
            //int campoSeleccion = (int)(poblacion.Count * porcentajeElitismo);

            // Elegir dos índices al azar de la lista de población actual.
            //int indice1 = random.Next(0, campoSeleccion);
            //int indice2 = random.Next(0, campoSeleccion);
            int indice1 = random.Next(0, poblacion.Count);
            int indice2 = random.Next(0, poblacion.Count);

            Individuo individuo1 = poblacion[indice1];
            Individuo individuo2 = poblacion[indice2];

            // Torneo
            if (individuo1.Fitness <= individuo2.Fitness)
            {
                seleccionado = individuo1;
                perdedor = individuo2;
            }
            else 
            { 
                seleccionado = individuo2;
                perdedor = individuo1;
            }

            // Elimina los individuos seleccionados.
            if (eliminarSeleccionados)
            {
                poblacion.Remove(seleccionado);
                poblacion.Remove(perdedor);
            }
            return seleccionado;
        }
    }
}
