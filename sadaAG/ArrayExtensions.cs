﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sadaAG
{
    static class ArrayExtensions
    {
        public static int[] CrearArrayEnteros(int largo, int valorInicial)
        {
            int[] resultado = new int[largo];

            for (int i=0, valor = valorInicial; i < largo; i++, valor++)
            {
                resultado[i] = valor;
            }

            return resultado;
        }

        public static void Split<T>(T[] array, int index, out T[] first, out T[] second)
        {
            first = array.Take(index).ToArray();
            second = array.Skip(index).ToArray();
        }
    }
}
