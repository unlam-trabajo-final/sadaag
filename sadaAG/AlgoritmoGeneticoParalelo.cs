﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace sadaAG
{
    public class AlgoritmoGeneticoParalelo : AlgoritmoGeneticoAbstracto
    {
        public System.Threading.Channels.Channel<bool> Canal { get; set; }

        /*public override bool ActualizarCanal()
        {

            // Toma el mejor individuo de la PoblacionTemporal
            Individuo individuo = PoblacionTemporal[0].Clonar();
            bool escribio = false, leyo = false;

            // Leer los mejores individuos de otras islas (procesos)
            List<Individuo> inmigrantes = new List<Individuo>();
            Task taskLectura = Task.Run(async () =>
            {
                while (await Canal.Reader.WaitToReadAsync())
                {
                    inmigrantes.Add(await Canal.Reader.ReadAsync());
                    leyo = true;
                }

            });

            // Escribir el mejor individuo en el Canal
            Task taskEscritura = Task.Run(async () =>
            {
                Console.WriteLine("Antes de escribir en el canal... ADN:" + individuo.ADN());
                await Canal.Writer.WriteAsync(individuo);
                Canal.Writer.Complete();
                Console.WriteLine("Completó la escritura.");
                escribio = true;
            });

            Task<bool> p = Task.WhenAll(taskLectura, taskEscritura).ContinueWith(_=> {
                Console.WriteLine("{2} Leyó {0} - Escribió {1}", leyo, escribio, Nombre);
                PoblacionTemporal.RemoveRange(PoblacionTemporal.Count - inmigrantes.Count, inmigrantes.Count);

                bool fitnessCero = false;
                inmigrantes.ForEach(i =>
                {
                    Console.Write("Cargando inmigrantes...");
                    PoblacionTemporal.Add(i.Clonar());
                    if (i.Fitness == 0)
                        fitnessCero = true;
                });
                if (fitnessCero)
                    Console.WriteLine("Encotrado!");
                return fitnessCero;
            });
            p.Wait();
            Console.WriteLine("Despues del wait.");
            return p.Result;
        }*/

        public override bool ActualizarCanal(bool fitnessCero)
        {

            // Toma el mejor individuo de la PoblacionTemporal
            //Individuo individuo = PoblacionTemporal[0].Clonar();
            //bool escribio = false, leyo = false;

            // Leer los mejores individuos de otras islas (procesos)
            //List<Individuo> inmigrantes = new List<Individuo>();

            Task taskEscritura = null;
            if (fitnessCero)
            {
                taskEscritura = Task.Factory.StartNew(() =>
                {
                    //Console.WriteLine("Antes de escribir en el canal... ADN:" + individuo.ADN());
                    Canal.Writer.TryWrite(fitnessCero);
                    //Canal.Writer.Complete();
                    //Console.WriteLine("Completó la escritura.");
                    //escribio = true;
                });
            }
            bool fitnessExtranjero = false;
            Task taskLectura = Task.Factory.StartNew( () =>
            {
                //while (await Canal.Reader.WaitToReadAsync())
                //{
                Canal.Reader.TryRead(out fitnessExtranjero);
                        //inmigrantes.Add(ind);
                //leyo = true;
                //}

            });
            if (fitnessCero)
                taskEscritura.Wait();
            taskLectura.Wait();

        
            //Console.WriteLine("{2} Leyó {0} - Escribió {1}", leyo, escribio, Nombre);
            //PoblacionTemporal.RemoveRange(PoblacionTemporal.Count - inmigrantes.Count, inmigrantes.Count);

            /*inmigrantes.ForEach(i =>
            {
                //Console.WriteLine("Cargando inmigrantes...");
                PoblacionTemporal.Add(i.Clonar());
                if (i.Fitness == 0)
                    fitnessCero = true;
            });*/
            if (fitnessCero)
                Console.WriteLine("Encotrado!");

            //Console.WriteLine("Despues del wait.");
            return (fitnessExtranjero | fitnessCero);
        }
    }
}
