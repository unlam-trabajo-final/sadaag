﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// La clase <c>Individuo</c> representa una solución completa, es decir una grilla con todas las asignaciones horarias de comisiones y aulas de toda la semana.
    /// </summary>
    public class Individuo : IComparable
    {
        /// <summary>
        /// El valor <c>Fitness</c> en nuestra implementación representa el nivel de error acumulado del <c>Individuo</c>.
        /// Los mejores individuos serán aquellos que tengan un valor bajo, tendiendo a cero.
        /// </summary>
        public int Fitness { get; set; }

        /// <summary>
        /// Contiene una lista de tuplas enteras [Id motivo, valor]
        /// </summary>
        public List<int[]> FitnessDesagregado { get; set; } = new List<int[]>();

        /// <summary>
        /// <c>Cromosoma</c> es una lista de <c>IGen</c>. A este nivel los genes son tambien listas de <c>IGen</c>.
        /// </summary>
        public List<IGen> Cromosoma { get; set; }

        /// <summary>
        /// Contiene una cantidad indeterminada a priori de penalizaciones, según haya infringido distintas normas.
        /// </summary>
        public Dictionary<string, double> Penalizaciones { get; set; } = new Dictionary<string, double>();

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            if (obj is Individuo otroIndividuo)
                return this.Fitness.CompareTo(otroIndividuo.Fitness);
            else
                throw new ArgumentException("El argumento no es del tipo Individuo.");
        }

        public override string ToString()
        {
            string resultado = string.Format("Fitness: {0}\n", Fitness) +
                                string.Format("Motivos de penalización: ");
            FitnessDesagregado.ForEach(m => resultado += "\n  " + IAlgoritmoGenetico.MotivosPenalizacion[m[0]] + ": " + m[1]);
            resultado += "\nAula: " + string.Join("\nAula: ", Cromosoma) + "\n";

            return resultado;
        }

        /*public Individuo Clonar()
        {
            List<IGen> nuevoCromosoma = new List<IGen>();
            foreach (IGen gen in this.Cromosoma)
            {
                nuevoCromosoma.Add(gen.Clonar());
            }

            return new Individuo()
            {
                Cromosoma = nuevoCromosoma,
                Fitness = this.Fitness
            };
        }*/
        public Individuo Clonar()
        {
            List<IGen> nuevoCromosoma = new List<IGen>();
            foreach (IGen gen in this.Cromosoma)
            {
                nuevoCromosoma.Add(gen.Clonar());
            }

            return new Individuo()
            {
                Cromosoma = nuevoCromosoma,
                Fitness = this.Fitness
            };
        }

        public string ADN()
        {
            return string.Join("|", Cromosoma);
        }
    }
}
