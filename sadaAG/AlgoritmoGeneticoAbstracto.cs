﻿using modelo;
using sadaAG.Resultados;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.ConstrainedExecution;

namespace sadaAG
{
    /// <summary>
    /// Es el objeto principal que coordina las distintas implementaciones y algoritmos de <c>Crossover>, FuncionFitness, Seleccion y Mutacion</c>.
    /// </summary>
    public abstract class AlgoritmoGeneticoAbstracto : IAlgoritmoGenetico
    {
        public string Nombre { get; set; }
        public ICrossover Crossover { get; set; }
        public IFuncionFitness FuncionFitness { get; set; } 
        public IMutacion Mutacion { get; set; }
        public ISeleccion Seleccion { get; set; }
        public List<Individuo> Poblacion { get; set; }
        public List<Individuo> PoblacionTemporal { get; set; } = new List<Individuo>();
        public int CantidadGeneraciones { get; set; }
        public int MejoresIndividuos { get; set; }
        public int CantidadIndividuos { get; set; }
        public bool Elitismo { get; set; }
        public Dataset Dataset { get; set; }
        public DatasetList DatasetList { get; set; }
        public IIndividuoFactory IndividuoFactory { get; set; }

        public event EventHandler<EvolucionInicioEventArgs> OnEvolucionInicio;
        public event EventHandler<EvolucionProgresoEventArgs> OnEvolucionProgreso;
        public event EventHandler<EvolucionFinEventArgs> OnEvolucionFin;

        private int GeneracionActual;
        private Individuo MejorHistorico;
        private readonly Random random = new Random();
        private const double DIVERSIDAD_GENETICA_MINIMA = 0.005;

        protected virtual void LanzarEvolucionInicioEvento(EvolucionInicioEventArgs e)
        {
            OnEvolucionInicio?.Invoke(this, e);
        }
        protected virtual void LanzarEvolucionProgresoEvento(EvolucionProgresoEventArgs e)
        {
            OnEvolucionProgreso?.Invoke(this, e);
        }
        protected virtual void LanzarEvolucionFinEvento(EvolucionFinEventArgs e)
        {
            OnEvolucionFin?.Invoke(this, e);
        }

        /// <summary>
        /// Inicializa la población de forma aleatoria al principio de la ejecución.
        /// </summary>
        public void InicializarPoblacion()
        {
            Poblacion = new List<Individuo>();

            for (int i=0; i < CantidadIndividuos; i++)
            {
                // Generar Individuo random.
                Individuo individuo = IndividuoFactory.CrearIndividuoRandom(DatasetList);

                // Agregarlo a la Población.
                Poblacion.Add(individuo);
                // Evaluarlo
                FuncionFitness.Evaluar(individuo);
            }
        }

        /// <summary>
        /// Corre la ejecución del <c>Algoritmo</c>.
        /// </summary>
        /// <returns>Devuelve una lista de <c>n</c> individuos que representan las mejores soluciones.</returns>
        public List<Individuo> Run()
        {
            // Empieza a meddir el tiempo
            Stopwatch cronometro = new Stopwatch();
            cronometro.Start();

            // Inicializar Población y calcular Fitness
            InicializarPoblacion();

            // Inicializar Generación actual
            GeneracionActual = 0;

            // Ordena la Población para ver si hay alguna solución
            Poblacion.Sort();

            // Se guarda el mejor individuo de todas las generaciones
            MejorHistorico = Poblacion[0].Clonar();

            // Ciclo de Evolución
            bool fitnessCero = Poblacion[0].Fitness == 0;
            bool fitnessCeroExtranjero = false;

            //MostrarResultados(GeneracionActual);

            double diversidadGenetica = CalcularDiversidadGenetica();

            while (!(fitnessCero || fitnessCeroExtranjero) && GeneracionActual < CantidadGeneraciones && diversidadGenetica > DIVERSIDAD_GENETICA_MINIMA)

            {
                // Incrementar Generación
                GeneracionActual++;

                // Limpiar la Población temporal.
                PoblacionTemporal.Clear();

                // Organizar la Poblacion
                Poblacion.Sort();

                // Se guarda el mejor de todos los tiempos
                if (Poblacion[0].Fitness < MejorHistorico.Fitness)
                    MejorHistorico = Poblacion[0].Clonar();

                // Calcula el tiempo restante
                long tiempoRestante = (long)(cronometro.Elapsed.TotalMilliseconds / GeneracionActual) * (CantidadGeneraciones - GeneracionActual);
                // Lanza el evento de progreso en la evolución
                EvolucionProgresoEventArgs eventArgs = new EvolucionProgresoEventArgs
                {
                    Datos = new EvolucionProgreso
                    {
                        Procesador = Nombre,
                        Generacion = GeneracionActual,
                        MejorFitnessActual = Poblacion[0].Fitness,
                        PeorFitnessActual = Poblacion[^1].Fitness,
                        MejorFitnessHistorico = MejorHistorico.Fitness,
                        PorcentajeCompletado = (GeneracionActual * 100.0) / CantidadGeneraciones,
                        MinutosRestantes = (int)(tiempoRestante / 1000) / 60,
                        SegundosRestantes = (int)(tiempoRestante / 1000) % 60,
                        MinutosTranscurridos = (int)cronometro.Elapsed.TotalMinutes,
                        SegundosTranscurridos = (int)(cronometro.Elapsed.TotalMilliseconds / 1000) % 60,
                        DiversidadGenetica = diversidadGenetica,
                        Poblacion = Poblacion.Count
                    }
                };
                LanzarEvolucionProgresoEvento(eventArgs);

                // Ejecutar operadores genéticos en toda la generación actual
                fitnessCero = EjecutarOperadoresGeneticos();

                // Envía su mejor individuo y recibe a los inmigrantes
                //fitnessCeroExtranjero = ActualizarCanal(fitnessCero);

                // Copiar nuevos individuos a la Población
                Poblacion.Clear();
                Poblacion.AddRange(PoblacionTemporal);

                // Actualiza la diversidad genética
                diversidadGenetica = CalcularDiversidadGenetica();
            }

            // Detiene el cronómetro
            cronometro.Stop();

            Poblacion.Sort();

            MostrarResultados();

            LanzarEvolucionFinEvento();

            // Devolver los mejores individuos
            return null;
        }
        private void MostrarResultados()
        {
            FuncionFitness.Evaluar(MejorHistorico);
            // Muestra mejor y peor individuo de la generación
            
            Individuo ultimo = Poblacion[Poblacion.Count-1];
            Console.WriteLine("\n*********[[{0} - GENERACION Nro. {1} - Tamaño Población: {2}]]********", Nombre, GeneracionActual, Poblacion.Count);
            Console.WriteLine("[MEJOR INDIVIDUO] {0}", Poblacion[0].ToString());
            //Console.WriteLine("[MEJOR INDIVIDUO HISTÓRICO] {0}", MejorHistorico.ToString());
        }
        protected virtual void LanzarEvolucionFinEvento()
        {
            SolucionBuilder builder = new SolucionBuilder(Poblacion[0], DatasetList);
            SolucionAG solucion = builder.SetProcesador(Nombre)
                                    .SetDias()
                                    .Crear();
            EvolucionFinEventArgs eventArgs = new EvolucionFinEventArgs
            {
                Solucion = solucion
            };
            LanzarEvolucionFinEvento(eventArgs);
        }
        private bool EjecutarOperadoresGeneticos()
        {
            bool fitnessCero = false;

            AplicarElitismo();

            Individuo madreAnterior = null;
            while (Poblacion.Count > 0 && PoblacionTemporal.Count < CantidadIndividuos && !fitnessCero)
            {

                // Seleccionar el padre para la reproducción
                Individuo padre = Seleccion.Seleccionar(Poblacion, true);
                Individuo madre;

                // Si Seleccionar va eliminando los individuos seleccionados de la Población, hay que verificar que la Población no esté vacía.
                if (Poblacion.Count > 0)
                {
                    madre = Seleccion.Seleccionar(Poblacion, out Individuo perdedor, false);

                    Poblacion.Remove(madre);
                    Poblacion.Remove(perdedor);
                    madreAnterior = madre;
                }
                else
                {
                    madre = madreAnterior;
                }
                // Ejecutar Crossover
                Individuo[] hijos = Crossover.Cruzar(padre, madre);
                Individuo[] hijos2 = Crossover.Cruzar(madre, padre);

                // Ejecutar Mutacion, elije al azar uno de los dos descendientes
                Mutacion.Mutar(hijos[random.Next(hijos.Length)]);
                Mutacion.Mutar(hijos2[random.Next(hijos2.Length)]);

                // Calcular Fitness
                FuncionFitness.Evaluar(hijos[0]);
                FuncionFitness.Evaluar(hijos[1]);
                FuncionFitness.Evaluar(hijos2[0]);
                FuncionFitness.Evaluar(hijos2[1]);

                // Agregar los nuevos individuos a la poblacion temporal
                PoblacionTemporal.Add(hijos[0]);
                PoblacionTemporal.Add(hijos[1]);
                PoblacionTemporal.Add(hijos2[0]);
                PoblacionTemporal.Add(hijos2[1]);
                
                fitnessCero = (hijos[0].Fitness == 0 | hijos[1].Fitness == 0 | hijos2[0].Fitness == 0 | hijos2[1].Fitness == 0);
            }
            return fitnessCero;
        }

        private void AplicarElitismo()
        {
            if (Elitismo)
            {
                PoblacionTemporal.Add(Poblacion[0]);
                Poblacion.RemoveAt(0);
            }
        }

        public double CalcularDiversidadGenetica()
        {
            double distintos = Poblacion.Select(ind => new { adn = ind.ADN() }).Distinct().Count();

            return (distintos / CantidadIndividuos) * 100;
        }

        private string VerificarHashes(Dictionary<int, List<IGen>> hashesCromosomas, List<IGen> cromosoma)
        {
            string mensajeHash = "";
            if (hashesCromosomas.ContainsKey(cromosoma.GetHashCode()))
            {
                mensajeHash += "Hash ya existente!";
                if (Object.ReferenceEquals(cromosoma, hashesCromosomas[cromosoma.GetHashCode()]))
                {
                    mensajeHash += " - Referencias iguales.";
                }
            }
            else
                hashesCromosomas.Add(cromosoma.GetHashCode(), cromosoma);

            return mensajeHash;
        }

        public virtual bool ActualizarCanal(bool fitnessCero)
        {
            return false;
        }
    }
}
