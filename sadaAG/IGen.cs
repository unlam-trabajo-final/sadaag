﻿using System;

namespace sadaAG
{
    /// <summary>
    /// <c>IGen</c> define la codificacion de un <c>Gen</c>. 
    /// Para nuestro caso de estudio, un <c>Individuo</c> posee cromosomas con genes, y estos genes a la vez poseen genes.
    /// </summary>
    public interface IGen
    {
        public IGen Clonar();

    }
}