﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// Un objeto <c>Comision</c> contiene la configuración completa de una comisión determinada.
    /// La comisión es la representación de un módulo conformado por instancia de curso que combina una Asignatura con un grupo 
    /// determinado de estudiantes.
    /// </summary>
    public class Comision : EntidadConPrestaciones
    {
        /// <summary>
        /// Identificador numérico único de la <c>Comision</c>.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// <c>Codigo</c> es referido a la codificación especifica que cada institución le asigna a sus comisiones.
        /// </summary>
        public string Codigo { get; set; }

        /// <summary>
        /// Representa la cantidad de unidades de tiempo que requiere esta comisión.
        /// </summary>
        public int Horas { get; set; }

        /// <summary>
        /// Este campo se utiliza para generar la población inicial. Va contando las horas que se fueron asignando.
        /// </summary>
        public int HorasAsignadas { get; set; }

        /// <summary>
        /// Es la cantidad de estudiantes inscriptos en esta comisión.
        /// </summary>
        public int Capacidad { get; set; }

        public string AsignaturaNombre { get; set; }

        public Comision()
        {
        }
        public Comision(int id, int horas)
        {
            this.Id = id;
            this.Horas = horas;
        }

    }
}
