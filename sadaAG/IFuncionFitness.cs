﻿using modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// Define la interfaz de la Funcion Fitness. Utilizamos el patron <c>Strategy</c> para tener la flexibilidad de cambiar distintos criterios de evaluación de los individuos.
    /// </summary>
    public interface IFuncionFitness
    {
        public Dataset dataset { get; set; }

        /// <summary>
        /// Evalua al <c>Individuo</c> según las reglas definidas en su imlementación.
        /// </summary>
        /// <param name="individuo">Objeto <c>Individuo</c> para evaluar. Una vez evaluado queda modificado su <c>Fitness</c>.</param>
        /// <returns>Devuelve el <c>Fitness</c> resultante de la evaluación.</returns>
        public int Evaluar(Individuo individuo);

    }
}
