﻿using sadaAG.Resultados;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace sadaAG
{
    public class EvolucionInicioEventArgs : EventArgs
    {
    }
    /// <summary>
    /// Contiene los argumentos del evento. Estos datos sirven para notificar al usuario del avance en la evolución del algoritmo.
    /// </summary>
    public class EvolucionProgresoEventArgs : EventArgs
    {
        public EvolucionProgreso Datos { get; set; }
    }
    public class EvolucionFinEventArgs : EventArgs
    {
        public string Procesador { get; set; }
        public SolucionAG Solucion { get; set; }
    }
    public class EvolucionProgreso
    {
        public string Procesador { get; set; }
        public int Generacion { get; set; }
        public int MejorFitnessActual { get; set; }
        public int PeorFitnessActual { get; set; }
        public int MejorFitnessHistorico { get; set; }
        public double PorcentajeCompletado { get; set; }
        public int MinutosRestantes { get; set; }
        public int SegundosRestantes { get; set; }
        public int MinutosTranscurridos { get; set; }
        public int SegundosTranscurridos { get; set; }
        public double DiversidadGenetica { get; set; }
        public int Poblacion { get; set; }

        public string ToCSV()
        {
            return String.Format("{0}|{1}|{2:F}|{3}|{4}|{5:F}|{6}",
                Procesador, Generacion, DiversidadGenetica, MejorFitnessActual, PeorFitnessActual, PorcentajeCompletado, (MinutosTranscurridos*60)+SegundosTranscurridos);
        }
        public override string ToString()
        {
            return String.Format("{0} Generación: {1,3} | Diversidad genética: {2,6:F}% | Mejor: {3,2} | Peor: {4,3} | Completado: {5,6:F}% | Transcurrido: {6,2:D2}m {7,2:D2}s | Finaliza en: {8,2:D2}m {9,2:D2}s | Población: {10}",
                Procesador, Generacion, DiversidadGenetica, MejorFitnessActual, PeorFitnessActual, PorcentajeCompletado, MinutosTranscurridos, SegundosTranscurridos, MinutosRestantes, SegundosRestantes, Poblacion);
        }
    }
}
