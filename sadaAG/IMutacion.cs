﻿using modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// Define la interfaz de los objetos <c>Mutacion</c>. Pueden existir varias implementaciones con criterios diferentes de mutación. 
    /// Se implementa aquí el patrón de diseño <c>Strategy</c>.
    /// </summary>
    public interface IMutacion
    {
        /// <summary>
        /// En esta variable se define la probabilidad de mutación que tiene cada individuo. Se recomienda siempre que sea un porcentaje bajo
        /// para no caer en una búsqueda mayoritariamente aleatoria.
        /// </summary>
        int Porcentaje { get; set; }

        public DatasetList Dataset { get; set; }

        /// <summary>
        /// Este método ejecuta el sorteo y la mutación de un individuo, basándose en el <c>Porcentaje</c> de mutación.
        /// </summary>
        /// <param name="individuo">Recibe la referencia de un objeto de tipo <c>Individuo</c>.</param>
        /// <returns>Devuelve el <c>Individuo</c> mutado, de salir seleccionado en el sorteo</returns>
        void Mutar(Individuo individuo);
    }
}
