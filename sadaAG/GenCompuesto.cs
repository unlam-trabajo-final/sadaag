﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// <c>Gen<Compuesto/c> una estructura de datos de <c>Genes</c> simples.
    /// </summary>
    public class GenCompuesto : IGen
    {
        /*public List<IGen> Valor { get ; set; }
        
        public GenCompuesto(List<IGen> valor)
        {
            this.Valor = valor;
        }
        */
        public IGen Clonar()
        {
            return new GenCompuesto((int[])Valor.Clone());
        }

        public int[] Valor { get; set; }

        public GenCompuesto(int[] valor)
        {
            this.Valor = valor;
        }

        public override string ToString()
        {
            return string.Join(',', Valor);
        }
    }
}
