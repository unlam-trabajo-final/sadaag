﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// <c>Gen</c> contiene la información básica de un <c>Cromosoma</c>.
    /// </summary>
    class Gen //: IGen
    {
        public int Valor { get ; set; }

        public Gen(int valor)
        {
            this.Valor = valor;
        }

        /*public IGen Clonar()
        {
            return new Gen(Valor);
        }*/

        public override string ToString()
        {
            return Valor.ToString();
        }
    }
}
