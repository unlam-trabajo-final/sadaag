﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using modelo;

namespace sadaAG
{
    public interface IAlgoritmoGenetico
    {

        public string Nombre { get; set; }
        /// <summary>
        /// Objeto encargado del cruce de cromosomas.
        /// </summary>
        public ICrossover Crossover { get; set; }

        /// <summary>
        /// Objeto que contiene el algoritmo de la función <c>Fitness</c> que evalúa la aptitud de cada individuo.
        /// </summary>
        public IFuncionFitness FuncionFitness { get; set; }

        /// <summary>
        /// Este objeto controla la mutación de los individuos.
        /// </summary>
        public IMutacion Mutacion { get; set; }

        /// <summary>
        /// Contiene el algoritmo de <c>Seleccion</c> de <c>Individuos</c>.
        /// </summary>
        public ISeleccion Seleccion { get; set; }

        /// <summary>
        /// Lista que mantiene en memoria la <c>Población</c> actual.
        /// </summary>
        public List<Individuo> Poblacion { get; set; }

        /// <summary>
        /// Es una lista que mantiene en memoria la población temporal. Se usa en el momento de cruzamiento y generación de nuevos individuos.
        /// </summary>
        public List<Individuo> PoblacionTemporal { get; set; }

        /// <summary>
        /// Indica el máximo de generaciones que debería iterar el algoritmo antes de finalizar.
        /// </summary>
        public int CantidadGeneraciones { get; set; }

        /// <summary>
        /// Es la cantidad de mejores soluciones que deberá devolver el algoritmo como resultado de su ejecución.
        /// </summary>
        public int MejoresIndividuos { get; set; }

        /// <summary>
        /// Cantidad de indivuduos por generación.
        /// </summary>
        public int CantidadIndividuos { get; set; }

        public bool Elitismo { get; set; }

        public Dataset Dataset { get; set; }
        public DatasetList DatasetList { get; set; }

        public IIndividuoFactory IndividuoFactory { get; set; }

        public static string[] MotivosPenalizacion { get; set; } = {
            "Por Horas excedentes",
            "Por Horas faltantes",
            "Por falta de Capacidad",
            "Por falta de Prestaciones",
            "Por no asignar Comisiones",
            "Por superposición horaria"
        };

        public event EventHandler<EvolucionInicioEventArgs> OnEvolucionInicio;
        public event EventHandler<EvolucionProgresoEventArgs> OnEvolucionProgreso;
        public event EventHandler<EvolucionFinEventArgs> OnEvolucionFin;

        /// <summary>
        /// Inicializa la población de forma aleatoria al principio de la ejecución.
        /// </summary>
        public void InicializarPoblacion();

        /// <summary>
        /// Corre la ejecución del <c>Algoritmo</c>.
        /// </summary>
        /// <returns>Devuelve una lista de <c>n</c> individuos que representan las mejores soluciones.</returns>
        public List<Individuo> Run();

        public double CalcularDiversidadGenetica();

        public bool ActualizarCanal(bool fitnessCero);

    }
}
