﻿using modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// Este objeto contiene las reglas para calcular el <c>Fitness</c> del <c>Individuo</c> provisto como parámetro.
    /// </summary>
    public class FuncionFitness : IFuncionFitness
    {
        private readonly static int PENA_CAPACIDAD_X_PERSONA = 1;
        private readonly static int PENA_X_HORA_EXCEDENTE = 1;
        private readonly static int PENA_X_HORA_FALTANTE = 5;
        private readonly static int PENA_X_COMISION_NO_ASIGNADA = 20;
        private readonly static int PENA_X_PUPERPOSICION_HORARIA = 5;


        public Dataset dataset { get; set; }

        public FuncionFitness(Dataset dataset)
        {
            this.dataset = dataset;
        }

        /// <summary>
        /// Calcula el valor de <c>Fitness</c> del <c>Individuo</c> y se lo asigna.
        /// </summary>
        /// <param name="individuo">Referencia a un objeto de tipo <c>Individuo</c>.</param>
        /// <returns>Devuelve el valor <c>Fitness</c> calculado.</returns>
        public int Evaluar(Individuo individuo)
        {
            // Diccionario de comisiones con los resultados del rastreo de Comisiones
            // La clave es el ID de la Comisión. 
            // El valor es un array de enteros con distintas sumatorias 
            //[horas asignadas (arranca con las horas requeridas en negativo), diferencia capacidad, diferencia prestaciones (este campo acumula directamente el valor final)]
            Dictionary<int, int[]> totalesPorComision = new Dictionary<int, int[]>();

            // Contiene la sumatoria de penalizaciones de falta de prestaciones por Aula/Comision
            // La clave está conformada por el Id de aula + Id de comisión: Ejemplo "1:3".
            Dictionary<string, int> penalizacionPrestaciones = new Dictionary<string, int>();

            //int totalSuperPosicionHoraria = 0;

            // Recorre el Cromosoma (compuesto de Aulas)
            for (int i = 0, aulaId = 1; i < individuo.Cromosoma.Count; i++, aulaId++)
            {
                Aula aula = dataset.Aulas.GetValueOrDefault<int, Aula>(aulaId, null);

                if (aula == null)
                    throw new Exception(String.Format("Aula Id={0} no encontrada.", aulaId));
                
                GenCompuesto gen = (GenCompuesto)individuo.Cromosoma[i];

                // Recorre el gen (horas asignadas, cada una a una Comision).
                //for (int j = 0, horaId = 1; j < gen.Valor.Count; j++, horaId++)
                for (int j = 0, horaId = 1; j < gen.Valor.Length; j++, horaId++)
                {
                    //int comisionId = ((Gen)gen.Valor[j]).Valor;
                    int comisionId = gen.Valor[j];

                    // Solo procesa si hay un Id de Comisión asignado.
                    if (comisionId > 0)
                    {
                        Comision comision = dataset.Comisiones.GetValueOrDefault<int, Comision>(comisionId, null);

                        if (comision == null)
                            throw new Exception(String.Format("Comision Id={0} no encontrada.", comisionId));

                        // Si no existe la Comisión en el rastreo, la agrega.
                        if (!totalesPorComision.ContainsKey(comision.Id))
                            totalesPorComision.Add(comision.Id, new int[] { -comision.Horas, 0, 0 });

                        // Incrementa el contador de horas asignadas
                        totalesPorComision[comision.Id][0]++;

                        // Verificar las prestaciones y la capacidad, sólo una vez por Aula/Comisión
                        VerificarPrestacionesyCapacidad(penalizacionPrestaciones, aula, comision, totalesPorComision);
                    }
                }
            }

            // Calcular el Fitness completo del individuo
            CalcularPenalizaciones(individuo, totalesPorComision, VerificarSuperposicionHoraria(individuo));

            return individuo.Fitness;
        }

        private int VerificarSuperposicionHoraria(Individuo individuo)
        {
            int total = 0;

            Dictionary<int, int> columna = new Dictionary<int, int>();

            // Recorre por columna
            //for (int col = 0; col < ((GenCompuesto)individuo.Cromosoma[0]).Valor.Count; col++)
            for (int col = 0; col < ((GenCompuesto)individuo.Cromosoma[0]).Valor.Length; col++)
            {
                // Recorre por fila
                foreach (GenCompuesto genAula in individuo.Cromosoma)
                {
                    //int clave = ((Gen)genAula.Valor[col]).Valor;
                    int clave = genAula.Valor[col];
                    if (clave != 0)
                    {
                        if (columna.ContainsKey(clave))
                            total++;
                        else
                            columna.Add(clave, 0);
                    }
                }
                columna.Clear();
            }
            
            return total;
        }

        /// <summary>
        /// Revisa una sola vez por combinación Aula/Comision las prestaciones y la capacidad.
        /// El resultado de las penalizaciones los multiplica por el peso puntual de cada prestación y registra en el diccionario.
        /// El también suma las unidades de capacidad faltante/sobrante, para luego calcular el valor final.
        /// </summary>
        /// <param name="penalizacionPrestaciones">Contiene los pares Aula/Comision con la sumatoria de penalizaciones, según la falta de prestaciones.</param>
        /// <param name="aula">Representa al Aula que se está evaluando.</param>
        /// <param name="comision">Representa a la Comisión que se está evaluando.</param>
        /// <param name="totalesPorComision">Contiene los totales de unidades de penalización por Comisión.</param>
        private void VerificarPrestacionesyCapacidad(Dictionary<string, int> penalizacionPrestaciones, Aula aula, Comision comision, Dictionary<int, int[]> totalesPorComision)
        {
            // Crear la clave combinando los IDs de Aula y Comision.
            string clave = String.Format("{0}:{1}", aula.Id.ToString(), comision.Id.ToString());

            // Si no existe la combinación (asignación), la creamos (una sola vez).
            if (!penalizacionPrestaciones.ContainsKey(clave))
            {
                penalizacionPrestaciones.Add(clave, 0);

                // Selecciona una sublista de las prestaciones no encontradas en el Aula y penaliza en base al peso de cada Prestación faltante.
                comision.Prestaciones
                    .FindAll(p => !aula.IdsPrestaciones.Contains(p.Id))
                    .ForEach(p => penalizacionPrestaciones[clave] += p.Peso);

                totalesPorComision[comision.Id][2] += penalizacionPrestaciones[clave];

                // Verificar que la capacidad de la Comision concuerde con la capacidad del Aula
                int capacidadDiferencia = comision.Capacidad - aula.Capacidad;
                if (capacidadDiferencia > 0)
                    totalesPorComision[comision.Id][1] += capacidadDiferencia;
            }
        }

        /// <summary>
        /// Recorre todos los totales guardados por Comisión y realiza los cálculos necesarios para totalizar el Fitness del Individuo.
        /// </summary>
        /// <param name="totalesPorComision">Contiene las penalizaciones obtenidas de revisar todas las asignaciones de Aula/Comision.</param>
        /// <returns>Devuelve el total de penalizaciones, este valor es equivalente al Fitness del individuo.</returns>
        private void CalcularPenalizaciones(Individuo individuo, Dictionary<int, int[]> totalesPorComision, int superposicionHoraria)
        {
            int totalHoraExcedente = 0;
            int totalHoraFaltante = 0;
            int totalCapacidad = 0;
            int totalPrestaciones = 0;
            int totalComisionesNoAsignadas = 0;

            // Totalizar las penalizaciones de las Comisiones asignadas
            foreach (KeyValuePair<int, int[]> elemento in totalesPorComision)
            {
                // Diferencia en cantidad de horas
                if (elemento.Value[0] > 0)
                    totalHoraExcedente += elemento.Value[0] * PENA_X_HORA_EXCEDENTE;
                if (elemento.Value[0] < 0)
                    totalHoraFaltante += -(elemento.Value[0] * PENA_X_HORA_FALTANTE);

                // Diferencia en capacidad
                totalCapacidad += elemento.Value[1] * PENA_CAPACIDAD_X_PERSONA;

                // Penalización en Prestaciones
                totalPrestaciones += elemento.Value[2];
            }

            // Verificar cuales Comisiones quedaron sin asignar 
            foreach (KeyValuePair<int,Comision> elem in dataset.Comisiones)
            {
                if (!totalesPorComision.ContainsKey(elem.Value.Id))
                    totalComisionesNoAsignadas += PENA_X_COMISION_NO_ASIGNADA;
            }

            int totalSuperposicionHoraria = superposicionHoraria * PENA_X_PUPERPOSICION_HORARIA;

            // Registra los motivos
            int[] totales = new int[] { totalHoraExcedente, totalHoraFaltante, totalCapacidad, totalPrestaciones, totalComisionesNoAsignadas, totalSuperposicionHoraria };

            for (int i = 0; i < totales.Length; i++)
            {
                if (individuo.FitnessDesagregado.Count - 1 < i)
                    individuo.FitnessDesagregado.Add(new int[] { i, totales[i] });
                else
                    individuo.FitnessDesagregado[i][1] = totales[i];
            }
            // Consolida el Fitness
            individuo.Fitness = totalCapacidad + totalPrestaciones + totalHoraExcedente + totalHoraFaltante + totalComisionesNoAsignadas + totalSuperposicionHoraria;
        }
    }
}
