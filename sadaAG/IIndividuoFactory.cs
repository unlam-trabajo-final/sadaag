﻿using modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    public interface IIndividuoFactory
    {
        public Individuo CrearIndividuoRandom(DatasetList dataset);

    }
}
