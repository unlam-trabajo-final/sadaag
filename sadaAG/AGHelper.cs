﻿using modelo;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;

namespace sadaAG
{
    public class AGHelper
    {
        public static List<IAlgoritmoGenetico> PrepararAlgoritmoGenetico(DatasetList datasetList)
        {
            Dictionary<string, string> argumentos = CargarArgumentos();

            Dataset dataset = datasetList.toDataset();

            // Verifica la relación entre la oferta de horas y la demanda
            int horasDemandadas = 0;
            datasetList.Comisiones.ForEach(c => horasDemandadas += c.Horas);
            int ofertaHoraria = datasetList.CantidadHorasSemanales * datasetList.Aulas.Count;
            int horasLibres = ofertaHoraria - horasDemandadas;

            // Determina el tipo de Fábrica de Individuos según las horas libres
            IIndividuoFactory individuoFactory = null;

            if (horasLibres > datasetList.Comisiones.Count * 4)
            {
                individuoFactory = new IndividuoFactoryDistribucionHomogenea();
            }
            else
            {
                individuoFactory = new IndividuoFactory();
            }

            // Crear el AlgoritmoGenetico y ejecutarlo
            AlgoritmoGeneticoBuilder builder = new AlgoritmoGeneticoBuilder()
                                        .SetCrossover(new Crossover())
                                        .SetFuncionFitness(new FuncionFitness(dataset))
                                        .SetMutacion(new Mutacion { Dataset = datasetList, Porcentaje = int.Parse(argumentos["mutacion"]) })
                                        .SetSeleccion(new Seleccion())
                                        .SetDataset(dataset)
                                        .SetDatasetList(datasetList)
                                        .SetCantidadIndividuos(int.Parse(argumentos["poblacion"]))
                                        .SetCantidadGeneraciones(int.Parse(argumentos["generaciones"]))
                                        .SetElitismo(bool.Parse(argumentos["elitismo"]))
                                        .SetIndividuoFactory(individuoFactory);

            // Si hay mas de 0 Hilos, se realiza una ejecución en paralelo
            int hilos = int.Parse(argumentos["hilos"]);
            List<IAlgoritmoGenetico> listaAG = builder.CrearAlgoritmosParalelos(hilos);

            return listaAG;
        }
        private static Dictionary<string, string> CargarArgumentos()
        {
            Dictionary<string, string> argumentos = new Dictionary<string, string>()
            {
                { "mutacion", "1" },
                { "poblacion", "3000" },
                { "generaciones", "200" },
                { "elitismo", "false" },
                { "hilos", "5" },
                { "salida", "PANTALLA" },
                { "archivo", "dataset_10com_12hs.json" },
                { "distribucion", "AUTOMATICA" }
            };
            return argumentos;
        }        
    }
}
