﻿using modelo;
using System.Collections.Generic;

namespace sadaAG.Resultados
{
    public class SolucionBuilder
    {
        private SolucionAG solucion;
        private Individuo individuo;
        private DatasetList dataset;

        public SolucionBuilder(Individuo individuo, DatasetList dataset)
        {
            solucion = new SolucionAG();
            solucion.Imprecision = individuo.Fitness;
            this.individuo = individuo;
            this.dataset = dataset;
        }
        public SolucionBuilder SetProcesador(string procesador)
        {
            solucion.Procesador = procesador;
            return this;
        }
        public SolucionBuilder SetDias()
        {
            solucion.Dias = GenerarDias();
            return this;
        }
        public SolucionBuilder SetId(int id)
        {
            solucion.Id = id;
            return this;
        }
        public SolucionAG Crear()
        {
            return solucion;
        }
        private IList<Dia> GenerarDias()
        {
            IList<Dia> dias = new List<Dia>();
            string[] nombresDias = { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" };
            string[] etiquetasHoras = { "8", "9", "10", "11", "19", "20", "21", "22"};
            int totalHoras = ((GenCompuesto)individuo.Cromosoma[0]).Valor.Length;
            int marcadorHora = 0;
            int horasDiarias = etiquetasHoras.Length;

            foreach (string nombreDia in nombresDias)
            {
                Dia dia = new Dia()
                {
                    Nombre = nombreDia,
                    Aulas = new List<Resultados.Aula>()
                };

                for (int indiceAula = 0; indiceAula < individuo.Cromosoma.Count; indiceAula++)
                {
                    GenCompuesto cromosoma = (GenCompuesto)individuo.Cromosoma[indiceAula];
                    Aula aula = new Aula()
                    {
                        Codigo = dataset.Aulas[indiceAula].Codigo,
                        Horas = new List<Hora>(),
                        Detalle = dataset.Aulas[indiceAula]
                    };
                    dia.Aulas.Add(aula);

                    for (int indiceHora = marcadorHora, indiceEtiquetasHoras = 0; 
                        indiceHora < marcadorHora + horasDiarias && indiceHora < totalHoras; 
                        indiceHora++, indiceEtiquetasHoras++)
                    {
                        string comisionCodigo = "", asignaturaNombre = "";

                        Comision comision = dataset.Comisiones.Find(c => c.Id == cromosoma.Valor[indiceHora]);
                        
                        if (comision != null)
                        {
                            comisionCodigo = comision.Codigo;
                            asignaturaNombre =  comision.AsignaturaNombre;
                        }

                        Hora hora = new Hora() { 
                            Orden = etiquetasHoras[indiceEtiquetasHoras],
                            Comision = comisionCodigo,
                            Asignatura = asignaturaNombre,
                            Detalle = comision,
                            Ok = true
                        };
                        
                        verificarAsignacion(hora, dataset.Aulas[indiceAula], comision);

                        aula.Horas.Add(hora);
                    }
                }

                if (dia.Aulas[0].Horas.Count > 0)
                    dias.Add(dia);

                marcadorHora += horasDiarias;
                if (marcadorHora > totalHoras)
                    break;
            }
            return dias;
        }
        private void verificarAsignacion(Hora hora, sadaAG.Aula aula, sadaAG.Comision comision)
        {

            if (comision != null)
            {

                IList<string> resultado = new List<string>();

                if (comision.Horas > comision.HorasAsignadas)
                    resultado.Add(string.Format("Faltan asignar {0} horas.", comision.HorasAsignadas - comision.Horas));

                foreach (Recurso recurso in comision.Prestaciones)
                {
                    if (!aula.Prestaciones.Contains(recurso))
                        resultado.Add(string.Format("Falta {0}", recurso.Nombre));
                }

                hora.Problemas = resultado;

                if (resultado.Count > 0)
                    hora.Ok = false;
            }
        }
    }
}
