﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG.Resultados
{
    public class Hora
    {
        public string Orden { get; set; }
        public string Comision { get; set; }
        public string Asignatura { get; set; }
        public sadaAG.Comision Detalle { get; set; }
        public bool Ok { get; set; }
        public IList<string> Problemas { get; set; }
    }
}