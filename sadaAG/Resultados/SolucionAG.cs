﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG.Resultados
{
    public class SolucionAG
    {
        public int Id { get; set; }
        public string Procesador { get; set; }
        public IList<Dia> Dias { get; set; }
        public int Imprecision { get; set; }
    }
}
