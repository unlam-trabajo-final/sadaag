﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG.Resultados
{
    public class Aula
    {
        public string Codigo { get; set; }
        public IList<Resultados.Hora> Horas { get; set; }
        public sadaAG.Aula Detalle { get; set; }
    }
}
