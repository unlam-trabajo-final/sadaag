﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG.Resultados
{
    public class Dia
    {
        public string Nombre { get; set; }
        public IList<Resultados.Aula> Aulas { get; set; }
    }
}
