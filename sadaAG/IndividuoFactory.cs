﻿using modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// <c>Factory</c> de objetos <c>Individuo</c>.
    /// </summary>
    public class IndividuoFactory : IIndividuoFactory
    {
        private readonly Random rnd = new Random();
        /// <summary>
        /// Crea una solución entera, al azar. Entre sus parámetros recibe una lista de <c>Comisiones</c>. Toma cada comisión y asiga todas
        /// sus horas de manera secuencial. Para asegurar la aleatoriedad, para cada cromosoma genera una lista distinta de índices para acceder a las aulas 
        /// en un orden distinto cada vez.
        /// </summary>
        /// <returns>Devuelve un nuevo <c>Individuo</c>.</returns>
        public Individuo CrearIndividuoRandom(DatasetList dataset)
        {
            List<IGen> cromosoma = new List<IGen>(dataset.Aulas.Count);

            // Limpia las horas asignadas de las Comisiones
            dataset.Comisiones.ForEach(com => com.HorasAsignadas = 0);

            // Genera un array con el orden de acceso aleatorio a las comisiones
            int[] indicesComisiones = ArrayExtensions.CrearArrayEnteros(dataset.Comisiones.Count, 0);
            rnd.Shuffle<int>(indicesComisiones);

            // Generar una pila con las comisiones mezcladas al azar
            Stack<Comision> pilaComisiones = new Stack<Comision>();
            foreach (int indice in indicesComisiones)
                pilaComisiones.Push(dataset.Comisiones[indice]);

            // Generar un array con el orden de acceso aleatoreo a las aulas
            int[] indicesAulas = ArrayExtensions.CrearArrayEnteros(dataset.Aulas.Count, 0);
            rnd.Shuffle<int>(indicesAulas);

            // Crear cromosoma con genes vacios (List de horarios para cada aula).
            for (int i = 0; i < dataset.Aulas.Count; i++)
                cromosoma.Add(null);

            // Generar la cantidad de Genes del cromosoma (Cada cromosoma es un aula con sus asignaciones).
            foreach (int indice in indicesAulas)
                cromosoma[indice] = CrearGenRandom(dataset.CantidadHorasSemanales, pilaComisiones);

            Individuo individuo = new Individuo
            {
                Cromosoma = cromosoma
            };

            return individuo;
        }

        /// <summary>
        /// Recibe una cantidad de horas y una pila de comisiones. Va colocando secuencialmente en cada elemento de la lista resultante, el Id de la comisión. El Id de la misma 
        /// comisión se repite por cada hora que necesita asignar esa Comisión.
        /// En el caso de acabarse el lugar en un Aula, se completa en otro Aula.
        /// </summary>
        /// <param name="cantidadHoras">Representa la cantidad de horas semanales que brinda cada <c>Aula</c>.</param>
        /// <param name="pilaComisiones">Es una pila que contiene las comisiones desordenadas a propósito.</param>
        /// <returns>Devuelve un objeto <c>IGen</c> que contiene una lista de valores enteros. Este Gen representa todo el horario de un <c>Aula</c>.</returns>
        private IGen CrearGenRandom(int cantidadHoras, Stack<Comision> pilaComisiones)
        {
            int[] asignaciones = new int[cantidadHoras];
            GenCompuesto gen = new GenCompuesto(new int[cantidadHoras]);

            Comision comision = null;
            int comisionId = 0;

            if (pilaComisiones.Count > 0)
            {
                comision = pilaComisiones.Peek();
                comisionId = comision.Id;
            }
            // Generar la cantidad de genes internos
            for (int i = 0; i < cantidadHoras; i++) /// Acá se puede cortar el ciclo a las 4 horas seguidas de la comisión
            {
                // Asignar Comisión
                gen.Valor[i] = comisionId;

                if (comision != null && comisionId != 0)
                {
                    comision.HorasAsignadas++;

                    // Si ya se asignaron todas las horas, se elimina esa comisión y se lee otra.
                    if (comision.HorasAsignadas == comision.Horas)
                    {
                        comisionId = 0;

                        if (pilaComisiones.Count > 0)
                            pilaComisiones.Pop();

                        if (pilaComisiones.Count > 0)
                        {
                            comision = pilaComisiones.Peek();
                            comisionId = comision.Id;
                        }
                    }
                }
            }
            return gen;
        }


        // Creaba el gen como una lista de genes simples
        /*private IGen CrearGenRandom(int cantidadHoras, Stack<Comision> pilaComisiones)
        {
            GenCompuesto gen = new GenCompuesto(new List<IGen>());

            Comision comision = null;
            int comisionId = 0;

            if (pilaComisiones.Count > 0)
            {
                comision = pilaComisiones.Peek();
                comisionId = comision.Id;
            }
            // Generar la cantidad de genes internos
            for (int i = 0; i < cantidadHoras; i++) /// Acá se puede cortar el ciclo a las 4 horas seguidas de la comisión
            {
                // Asignar Comisión
                gen.Valor.Add(new Gen(comisionId));

                if (comision != null && comisionId != 0)
                {
                    comision.HorasAsignadas++;

                    // Si ya se asignaron todas las horas, se elimina esa comisión y se lee otra.
                    if (comision.HorasAsignadas == comision.Horas)
                    {
                        comisionId = 0;

                        if (pilaComisiones.Count > 0)
                            pilaComisiones.Pop();

                        if (pilaComisiones.Count > 0)
                        {
                            comision = pilaComisiones.Peek();
                            comisionId = comision.Id;
                        }
                    }
                }
            }
            return gen;
        }*/
    }
}
