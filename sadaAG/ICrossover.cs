﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sadaAG
{
    /// <summary>
    /// Siguiendo el patrón de diseño <c>Strategy</c>, <c>ICrossover</c> define la interfaz de los objetos que implementan distintos algoritmos
    /// o criterios para el cruce de <c>Individuos</c>.
    /// </summary>
    public interface ICrossover
    {
        /// <summary>
        /// Este método realiza el cruce de los dos individuos pasados por parámetro.
        /// </summary>
        /// <param name="individuo1">Individuo padre.</param>
        /// <param name="individuo2">Individuo madre.</param>
        /// <returns>Retorna un array con dos Individuos nuevos, generados a partir de sus padres.</returns>
        Individuo[] Cruzar(Individuo individuo1, Individuo individuo2);
    }
}
