﻿using System;
using System.Collections.Generic;

namespace sadaAG
{
    /// <summary>
    /// Representa un aula. Contiene todas sus características y prestaciones.
    /// </summary>
    public class Aula : EntidadConPrestaciones
    {
        /// <summary>
        /// <c>Id</c> es el código del Aula.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// <c>Codigo</c> es referido a la codificación especifica que cada institución le asigna a sus aulas.
        /// </summary>
        public string Codigo { get; set; }

        /// <summary>
        /// La capacidad en cantidad de personas que soporta el Aula.
        /// </summary>
        public int Capacidad { get; set; }

        public Aula()
        {
        }
        public Aula(int id)
        {
            this.Id = id;
        }
    }
}
