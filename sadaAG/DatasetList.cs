﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using sadaAG;

namespace modelo
{
    /// <summary>
    /// Esta clase agrupa toda la representación en memoria de los datos de entrada. Debe ser poblada por objetos obtenidos de alguna fuente externa (preferiblemente un archivo json).
    /// </summary>
    public class DatasetList
    {
        /// <summary>
        /// <c>Diccionario</c>, contiene la lista de objetos <c>Aula</c> provisto por el cliente que consume este servicio.
        /// </summary>
        public List<Aula> Aulas { get; set; } = new List<Aula>();

        /// <summary>
        /// <c>Diccionario</c>, contiene la lista de objetos <c>Comision</c> provisto por el cliente que consume este servicio.
        /// </summary>
        public List<Comision> Comisiones { get; set; } = new List<Comision>();

        /// <summary>
        /// <c>Diccionario</c>, contiene la lista de objetos <c>IPrestacion</c> provisto por el cliente que consume este servicio.
        /// </summary>
        public List<Recurso> Prestaciones { get; set; } = new List<Recurso>();

        public int CantidadHorasSemanales { get; set; } = 12;

        /// <summary>
        /// Carga las listas de referencias a objetos que tiene cada entidad, en base a la lista de IDs.
        /// </summary>
        public void CargarObjetos()
        {
            // Carga las referencias a Prestaciones de cada Aula.
            Aulas.ForEach(a => a.CargarPrestaciones(Prestaciones));
            Comisiones.ForEach(c => c.CargarPrestaciones(Prestaciones));
        }

        public Dataset toDataset()
        {
            Dataset dataset = new Dataset();
            // Carga los diccionarios
            dataset.Aulas = Aulas.ToDictionary(x => x.Id, x => x);
            dataset.Comisiones = Comisiones.ToDictionary(x => x.Id, x => x);
            dataset.Prestaciones = Prestaciones.ToDictionary(x => x.Id, x => x);
            dataset.CantidadHorasSemanales = CantidadHorasSemanales;
            return dataset;
        }
    }
}
