﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sadaWeb.Datos;

namespace sadaWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SolucionesController : ControllerBase
    {
        private readonly sadaContext _context;

        public SolucionesController(sadaContext context)
        {
            _context = context;
        }

        // GET: api/Soluciones
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Solucion>>> GetSolucions()
        {
            return await _context.Solucions.ToListAsync();
        }

        // GET: api/Soluciones/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Solucion>> GetSolucion(int id)
        {
            var solucion = await _context.Solucions.FindAsync(id);

            if (solucion == null)
            {
                return NotFound();
            }

            return solucion;
        }

        // PUT: api/Soluciones/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSolucion(int id, Solucion solucion)
        {
            if (id != solucion.Id)
            {
                return BadRequest();
            }

            _context.Entry(solucion).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SolucionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Soluciones
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Solucion>> PostSolucion(Solucion solucion)
        {
            _context.Solucions.Add(solucion);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSolucion", new { id = solucion.Id }, solucion);
        }

        // DELETE: api/Soluciones/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Solucion>> DeleteSolucion(int id)
        {
            var solucion = await _context.Solucions.FindAsync(id);
            if (solucion == null)
            {
                return NotFound();
            }

            _context.Solucions.Remove(solucion);
            await _context.SaveChangesAsync();

            return solucion;
        }

        private bool SolucionExists(int id)
        {
            return _context.Solucions.Any(e => e.Id == id);
        }
    }
}
