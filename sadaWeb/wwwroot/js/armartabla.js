﻿"use strict";
var dataset;

function crearPresentacion(objeto) {

    dataset = objeto;

    var presentacion = document.createElement("div");

    objeto.Dias.forEach(dia => {
        var cardshadow = document.createElement("div");
        cardshadow.className = "card shadow mb-4";


        var cardheader = document.createElement("div");
        cardheader.className = "card-header py-3";

        var titulo = document.createElement("h6");
        titulo.className = "m-0 font-weight-bold text-primary";
        titulo.innerText = dia.Nombre;

        cardheader.appendChild(titulo);
        cardshadow.appendChild(cardheader);

        cardshadow.appendChild(cardheader);
        cardshadow.appendChild(crearDia(dia));

        presentacion.appendChild(cardshadow);
    });

    return presentacion;
}

function crearDia(dia) {

    var cardbody = document.createElement("div");
    cardbody.className = "card-body";

    var tableresponsive = document.createElement("div");
    tableresponsive.className = "table-responsive";

    var tabla = document.createElement("table");
    tabla.id = dia.Nombre;
    tabla.className = "table table-bordered";
    tabla.width = "100%";
    tabla.cellSpacing = "0";

    var cabecera = crearCabecera(dia);
    var cuerpo = crearCuerpo(dia);

    tabla.appendChild(cabecera);
    tabla.appendChild(cuerpo);

    tableresponsive.appendChild(tabla);
    cardbody.appendChild(tableresponsive);

    return cardbody;
}

function crearCabecera(dia) {
    var cabecera = document.createElement("thead");
    var registro = document.createElement("tr");

    var celda = document.createElement("th");
    celda.scope = "col";
    celda.innerText = "Aula";
    registro.appendChild(celda);

    dia.Aulas[0].Horas.forEach(hora => {
        celda = document.createElement("th");
        celda.scope = "col";
        celda.innerText = hora.Orden + "hs";
        registro.appendChild(celda);
    });
    cabecera.appendChild(registro);

    return cabecera;
}

function crearCuerpo(dia) {
    var registros = document.createElement("tbody");
    dia.Aulas.forEach(aula => {

        var registro = document.createElement("tr");
        registro.className = "sorteable";

        var celda = document.createElement("th");
        celda.innerText = aula.Codigo;
        celda.scope = "row";
        celda.style.cursor = "help";
        celda.id = dia.Nombre + "." + aula.Codigo;

        var detallesAula = "<ul><li>Capacidad para " + aula.Detalle.Capacidad + " personas </li>";
        aula.Detalle.Prestaciones.forEach(prestacion => detallesAula += "<li>" + prestacion.Nombre + "</li>");
        detallesAula += "</ul>";
        celda.setAttribute("data-toggle", "popover");
        celda.setAttribute("title", "Recursos disponibles");
        celda.setAttribute("data-content", detallesAula);
        celda.setAttribute("data-html", "true");
        $("#" + celda.id).tooltip();

        registro.appendChild(celda);

        aula.Horas.forEach(hora => {
            celda = document.createElement("td");
            celda.className = "ui-sortable-handle";
            celda.id = dia.Nombre + "." + aula.Codigo + "." + hora.orden;
            celda.setAttribute("data-toggle", "popover");


            if (hora.Detalle != null) {
                celda.innerText = hora.Comision + " " + hora.Asignatura;

                celda.setAttribute("title", "Requerimientos");
                celda.setAttribute("data-html", "true");

                var requerimientos = "<ul><li>Capacidad para " + hora.Detalle.Capacidad + " personas </li>";
                hora.Detalle.Prestaciones.forEach(requerimiento => requerimientos += "<li>" + requerimiento.Nombre + " (Importancia: " + requerimiento.Peso + ")" + "</li>");
                requerimientos += "</ul>";

                
                var tarjeta = requerimientos;

                if (!hora.Ok) {

                    var problemas = "<h6>Inconvenientes</h6>";
                    problemas += "<ul>";
                    hora.Problemas.forEach(problema => problemas += "<li>" + problema + "</li>");
                    problemas += "</ul>";
                    tarjeta += problemas;


                    var pulgar = document.createElement("i");
                    pulgar.id = dia.Nombre + "." + aula.Codigo + "." + hora.orden;
                    pulgar.className = "fa fa-thumbs-down";
                    pulgar.setAttribute("aria-hidden", "true");

                    var divPulgar = document.createElement("div");
                    divPulgar.className = "text-warning text-center";
                    divPulgar.appendChild(pulgar);

                    celda.style.cursor = "help";
                    celda.appendChild(divPulgar);
                }

                celda.setAttribute("data-content", tarjeta);

                $("#" + celda.id).tooltip();
            }







            /*if (!hora.Ok) {
                var problemas = "<ul>";
                hora.Problemas.forEach(problema => problemas += "<li>" + problema + "</li>");
                problemas += "</ul>";

                var pulgar = document.createElement("i");
                pulgar.id = dia.Nombre + "." + aula.Codigo + "." + hora.orden;
                pulgar.className = "fa fa-thumbs-down";
                pulgar.setAttribute("aria-hidden", "true");

                celda.setAttribute("title", "Inconvenientes");
                celda.setAttribute("data-html", "true");
                celda.style.cursor = "help";

                var requerimientos = "<h6>Requerimientos</h6>";
                requerimientos += "<ul>";
                hora.Detalle.Prestaciones.forEach(requerimiento => requerimientos += "<li>" + requerimiento.Nombre + " (Importancia: " + requerimiento.Peso + ")" + "</li>");
                requerimientos += "</ul>";

                var tarjeta = problemas + requerimientos;

                celda.setAttribute("data-content", tarjeta);

                $("#" + celda.id).tooltip();

                var divPulgar = document.createElement("div");
                divPulgar.className = "text-warning text-center";
                divPulgar.appendChild(pulgar);

                celda.appendChild(divPulgar);
            }*/
            registro.appendChild(celda);
        });
        registros.appendChild(registro);
    });

    return registros;
}
