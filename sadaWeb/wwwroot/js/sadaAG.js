﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/AlgoritmoGeneticoHub").build();

document.getElementById("ejecutarAlgoritmo").disabled = true;

mostrarSpinner(false);

var procesosFinalizados = 0;

function mostrarSpinner(mostrar) {
    var valor = "none";
    if (mostrar)
        valor = "block";

    document.getElementById("spinner").style.display = valor;
}

connection.on("EvolucionProgreso", function (datos) {
    var objeto = JSON.parse(datos);
    var segundos = "0" + objeto.SegundosRestantes;
    var texto = objeto.MinutosRestantes + "'" + segundos.substring(segundos.length-2) + "\"";
    document.getElementById(objeto.Procesador).innerText = texto;

    actualizarProgressBar(objeto.Procesador, objeto.PorcentajeCompletado);

    mostrarSpinner(false);
});

function actualizarProgressBar(procesador, porcentaje) {
    $("#B" + procesador)
        .css("width", porcentaje + "%")
        .attr("aria-valuenow", porcentaje)
        .text(porcentaje + "%");
}

connection.on("EvolucionFin", function (solucion) {
    
    var objSolucion = JSON.parse(solucion);

    document.getElementById(objSolucion.Procesador).innerText = "0'00\"";
    actualizarProgressBar(objSolucion.Procesador, 100);

    document.getElementById("link" + objSolucion.Procesador).setAttribute("href", "Solucion?id=" + objSolucion.Id);
    document.getElementById("verResultado" + objSolucion.Procesador).style.display = "block";

    var pulgar = document.createElement("i");
    if (objSolucion.Imprecision === 0) {
        pulgar.className = "fa fa-thumbs-up text-success";
        pulgar.setAttribute("aria-hidden", "true");
    }
    else {
        pulgar.className = "fa fa-thumbs-down text-warning";
        pulgar.setAttribute("aria-hidden", "true");
    }
    document.getElementById("pulgar" + objSolucion.Procesador).appendChild(pulgar);

    verificarFin();

});


function verificarFin() {

    if (procesosFinalizados == 4) {
        procesosFinalizados = 0;
        document.getElementById("limpiar").style.display = "block";
        document.getElementById("contenedorProcesar").style.display = "none";
    }

    procesosFinalizados++;
}

connection.start().then(function () {
    document.getElementById("ejecutarAlgoritmo").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("ejecutarAlgoritmo").addEventListener("click", function (event) {
    connection.invoke("Ejecutar", 1, 1, horasSeleccionadas()).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();

    mostrarSpinner(true);
});

function horasSeleccionadas() {
    
    var horas = $("#selectHoras").val();
    
    return parseInt(horas);

}
