﻿"use strict";

window.onload = function () {
    $.getJSON("/api/Soluciones/" + document.getElementById("solucionId").getAttribute("value"),
        data => {
            var fecha = new Date(data.fechaEjecucion);
            document.getElementById("resultado").innerText = "Fecha de ejecución: " + fecha.getDate() + "/" + fecha.getMonth() + "/" + fecha.getFullYear() + " - Imprecisión: " + data.imprecision;
            var solucion = JSON.parse(data.json);

            document.getElementById("resultado").appendChild(crearPresentacion(solucion));

            aplicarSortable();

            document.getElementById("spinner").style.display = "none";
        });
};


function aplicarSortable() {
    $('.sortable').sortable({
        items: 'tr'
    });
    $('tbody').sortable({
        items: 'td'
    });

    $('[data-toggle="popover"]').popover();
}

function exportarExcelv2() {
    const workbook = XLSX.utils.book_new();

    const tablas = document.getElementsByTagName("Table");
    const listaTablas = Array.prototype.slice.call(tablas);

    listaTablas.forEach(
        tabla => {
            const ws1 = XLSX.utils.table_to_sheet(tabla);
            XLSX.utils.book_append_sheet(workbook, ws1, tabla.id);
        });

    XLSX.writeFile(workbook, "SADA-Cronograma.xlsx");
} 