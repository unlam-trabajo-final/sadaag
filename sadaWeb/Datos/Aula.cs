﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Aula
    {
        public Aula()
        {
            Aula_Recursos = new HashSet<Aula_Recurso>();
        }

        public int Id { get; set; }
        public string Codigo { get; set; }
        public int Capacidad { get; set; }
        public int IdInstitucion { get; set; }

        public virtual Institucion IdInstitucionNavigation { get; set; }
        public virtual ICollection<Aula_Recurso> Aula_Recursos { get; set; }
    }
}
