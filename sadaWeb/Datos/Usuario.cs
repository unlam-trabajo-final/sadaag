﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Usuario
    {
        public Usuario()
        {
            Usuario_Institucions = new HashSet<Usuario_Institucion>();
        }

        public int Id { get; set; }
        public string Apellido { get; set; }
        public string Nombres { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public virtual ICollection<Usuario_Institucion> Usuario_Institucions { get; set; }
    }
}
