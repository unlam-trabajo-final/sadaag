﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Asignatura
    {
        public Asignatura()
        {
            Comisions = new HashSet<Comision>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdInstitucion { get; set; }

        public virtual Institucion IdInstitucionNavigation { get; set; }
        public virtual ICollection<Comision> Comisions { get; set; }
    }
}
