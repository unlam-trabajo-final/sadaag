﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using sadaWeb;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class sadaContext : DbContext
    {
        public sadaContext()
        {
        }

        public sadaContext(DbContextOptions<sadaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Ambito> Ambitos { get; set; }
        public virtual DbSet<Asignatura> Asignaturas { get; set; }
        public virtual DbSet<Aula> Aulas { get; set; }
        public virtual DbSet<Aula_Recurso> Aula_Recursos { get; set; }
        public virtual DbSet<Comision> Comisions { get; set; }
        public virtual DbSet<Comision_Recurso> Comision_Recursos { get; set; }
        public virtual DbSet<Institucion> Institucions { get; set; }
        public virtual DbSet<Nivel> Nivels { get; set; }
        public virtual DbSet<Recurso> Recursos { get; set; }
        public virtual DbSet<Solucion> Solucions { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Usuario_Institucion> Usuario_Institucions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-2R5TQIV;Database=sada;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ambito>(entity =>
            {
                entity.ToTable("Ambito");

                entity.HasIndex(e => e.Nombre, "IX_Nombre");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Asignatura>(entity =>
            {
                entity.ToTable("Asignatura");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdInstitucionNavigation)
                    .WithMany(p => p.Asignaturas)
                    .HasForeignKey(d => d.IdInstitucion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Asigantura_Institucion");
            });

            modelBuilder.Entity<Aula>(entity =>
            {
                entity.ToTable("Aula");

                entity.HasIndex(e => e.Codigo, "IX_Codigo");

                entity.HasIndex(e => e.IdInstitucion, "IX_IdInstitucion");

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.IdInstitucionNavigation)
                    .WithMany(p => p.Aulas)
                    .HasForeignKey(d => d.IdInstitucion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Aula_Institucion");
            });

            modelBuilder.Entity<Aula_Recurso>(entity =>
            {
                entity.HasKey(e => new { e.IdAula, e.IdRecurso });

                entity.ToTable("Aula_Recurso");

                entity.HasOne(d => d.IdAulaNavigation)
                    .WithMany(p => p.Aula_Recursos)
                    .HasForeignKey(d => d.IdAula)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Aula_Recurso_Aula");

                entity.HasOne(d => d.IdRecursoNavigation)
                    .WithMany(p => p.Aula_Recursos)
                    .HasForeignKey(d => d.IdRecurso)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Aula_Recurso_Recurso");
            });

            modelBuilder.Entity<Comision>(entity =>
            {
                entity.ToTable("Comision");

                entity.HasIndex(e => e.Codigo, "IX_ComisionCodigo");

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.IdAsignaturaNavigation)
                    .WithMany(p => p.Comisions)
                    .HasForeignKey(d => d.IdAsignatura)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comision_Asigantura");
            });

            modelBuilder.Entity<Comision_Recurso>(entity =>
            {
                entity.HasKey(e => new { e.IdComision, e.IdRecurso });

                entity.ToTable("Comision_Recurso");

                entity.HasOne(d => d.IdComisionNavigation)
                    .WithMany(p => p.Comision_Recursos)
                    .HasForeignKey(d => d.IdComision)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comision_Recurso_Comision");

                entity.HasOne(d => d.IdRecursoNavigation)
                    .WithMany(p => p.Comision_Recursos)
                    .HasForeignKey(d => d.IdRecurso)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comision_Recurso_Recurso");
            });

            modelBuilder.Entity<Institucion>(entity =>
            {
                entity.ToTable("Institucion");

                entity.Property(e => e.CUIT)
                    .HasMaxLength(11)
                    .IsFixedLength(true);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Telefono).HasMaxLength(50);

                entity.HasOne(d => d.IdAmbitoNavigation)
                    .WithMany(p => p.Institucions)
                    .HasForeignKey(d => d.IdAmbito)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Institucion_Ambito");

                entity.HasOne(d => d.IdNivelNavigation)
                    .WithMany(p => p.Institucions)
                    .HasForeignKey(d => d.IdNivel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Institucion_Nivel");
            });

            modelBuilder.Entity<Nivel>(entity =>
            {
                entity.ToTable("Nivel");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Recurso>(entity =>
            {
                entity.ToTable("Recurso");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Solucion>(entity =>
            {
                entity.ToTable("Solucion");

                entity.Property(e => e.FechaEjecucion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Imprecision);

                entity.HasOne(d => d.IdInstitucionNavigation)
                    .WithMany(p => p.Solucions)
                    .HasForeignKey(d => d.IdInstitucion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Solucion_Institucion");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.ToTable("Usuario");

                entity.Property(e => e.Apellido)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.password)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Usuario_Institucion>(entity =>
            {
                entity.HasKey(e => new { e.IdUsuario, e.IdInstitucion });

                entity.ToTable("Usuario_Institucion");

                entity.HasOne(d => d.IdInstitucionNavigation)
                    .WithMany(p => p.Usuario_Institucions)
                    .HasForeignKey(d => d.IdInstitucion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Usuario_Institucion_Institucion");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Usuario_Institucions)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Usuario_Institucion_Usuario");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
