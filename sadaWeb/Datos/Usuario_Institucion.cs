﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Usuario_Institucion
    {
        public int IdUsuario { get; set; }
        public int IdInstitucion { get; set; }

        public virtual Institucion IdInstitucionNavigation { get; set; }
        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
