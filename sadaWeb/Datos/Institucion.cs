﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Institucion
    {
        public Institucion()
        {
            Asignaturas = new HashSet<Asignatura>();
            Aulas = new HashSet<Aula>();
            Solucions = new HashSet<Solucion>();
            Usuario_Institucions = new HashSet<Usuario_Institucion>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string CUIT { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int IdNivel { get; set; }
        public int IdAmbito { get; set; }

        public virtual Ambito IdAmbitoNavigation { get; set; }
        public virtual Nivel IdNivelNavigation { get; set; }
        public virtual ICollection<Asignatura> Asignaturas { get; set; }
        public virtual ICollection<Aula> Aulas { get; set; }
        public virtual ICollection<Solucion> Solucions { get; set; }
        public virtual ICollection<Usuario_Institucion> Usuario_Institucions { get; set; }
    }
}
