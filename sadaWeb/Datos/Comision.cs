﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Comision
    {
        public Comision()
        {
            Comision_Recursos = new HashSet<Comision_Recurso>();
        }

        public int Id { get; set; }
        public string Codigo { get; set; }
        public int Horas { get; set; }
        public int Capacidad { get; set; }
        public int IdAsignatura { get; set; }

        public virtual Asignatura IdAsignaturaNavigation { get; set; }
        public virtual ICollection<Comision_Recurso> Comision_Recursos { get; set; }
    }
}
