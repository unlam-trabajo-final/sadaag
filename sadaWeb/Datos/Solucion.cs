﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Solucion
    {
        public int Id { get; set; }
        public string JSON { get; set; }
        public DateTime FechaEjecucion { get; set; }
        public int IdInstitucion { get; set; }
        public int Imprecision { get; set; }
        public virtual Institucion IdInstitucionNavigation { get; set; }
    }
}
