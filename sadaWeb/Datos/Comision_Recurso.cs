﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Comision_Recurso
    {
        public int IdComision { get; set; }
        public int IdRecurso { get; set; }

        public virtual Comision IdComisionNavigation { get; set; }
        public virtual Recurso IdRecursoNavigation { get; set; }
    }
}
