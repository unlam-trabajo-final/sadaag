﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Recurso
    {
        public Recurso()
        {
            Aula_Recursos = new HashSet<Aula_Recurso>();
            Comision_Recursos = new HashSet<Comision_Recurso>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Peso { get; set; }

        public virtual ICollection<Aula_Recurso> Aula_Recursos { get; set; }
        public virtual ICollection<Comision_Recurso> Comision_Recursos { get; set; }
    }
}
