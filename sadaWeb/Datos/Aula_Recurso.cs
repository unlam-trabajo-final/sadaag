﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Aula_Recurso
    {
        public int IdAula { get; set; }
        public int IdRecurso { get; set; }

        public virtual Aula IdAulaNavigation { get; set; }
        public virtual Recurso IdRecursoNavigation { get; set; }
    }
}
