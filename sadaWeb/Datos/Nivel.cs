﻿using System;
using System.Collections.Generic;

#nullable disable

namespace sadaWeb.Datos
{
    public partial class Nivel
    {
        public Nivel()
        {
            Institucions = new HashSet<Institucion>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Institucion> Institucions { get; set; }
    }
}
