﻿using Microsoft.AspNetCore.SignalR;
using sadaAG;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using sadaWeb.Servicios;
using modelo;
using sadaAG.Resultados;
using System;

namespace sadaWeb.Hubs
{
    public class AlgoritmoGeneticoHub : Hub
    {
        private IHubContext<AlgoritmoGeneticoHub> _hubcontext;
        private string ConnectionId;
        public AlgoritmoGeneticoHub(IHubContext<AlgoritmoGeneticoHub> hc)
        {
            _hubcontext = hc;   
        }

        public override Task OnConnectedAsync()
        {
            this.ConnectionId = Context.ConnectionId;

            return base.OnConnectedAsync();
        }

        public async Task Ejecutar(int idUsuario, int idInstitucion, int horasSemanales)
        {
            Repositorio repositorio = new Repositorio(idUsuario, idInstitucion);
            DatasetList datasetList = repositorio.ObtenerDatasetList();
            datasetList.CantidadHorasSemanales = horasSemanales;

            List<IAlgoritmoGenetico> hilosAG = AGHelper.PrepararAlgoritmoGenetico(datasetList);

            Parallel.ForEach(hilosAG, (a) =>
            {
                
                a.OnEvolucionProgreso += async (sender, e) => await Clients.Caller.SendAsync("EvolucionProgreso", JsonSerializer.Serialize(e.Datos));

                a.OnEvolucionFin += async (sender, e) => {
                    string conId = Context.ConnectionId;
                    Repositorio repo = new Repositorio(idUsuario, idInstitucion);
                    string json = await repo.GuardarSolucion(e.Solucion);
                    await _hubcontext.Clients.Client(conId).SendAsync("EvolucionFin", json);
                };

                a.Run();

            });

        }
    }

    public class ContextoHub<THub> : IHubContext<Hub>
    {
        public IHubClients Clients => this.Clients;

        public IGroupManager Groups => this.Groups;

        public string ConnectionId { get; set; }
    }
}