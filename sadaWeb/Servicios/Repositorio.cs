﻿using Microsoft.EntityFrameworkCore;
using modelo;
using sadaAG.Resultados;
using sadaWeb.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace sadaWeb.Servicios
{
    public class Repositorio : IDisposable
    {
        private sadaContext contexto = new sadaContext();
        private int idUsuario;
        private int idInstitucion;
        
        public Repositorio(int idUsuario, int idInstitucion)
        {
            this.idUsuario = idUsuario;
            this.idInstitucion = idInstitucion;
        }
        public DatasetList ObtenerDatasetList()
        {
            DatasetList datasetList = new DatasetList();

            datasetList.Aulas = CargarAulas();
            datasetList.Comisiones = CargarComisiones();
            datasetList.Prestaciones = CargarRecursos();

            datasetList.CargarObjetos();

            return datasetList;
        }
        private List<sadaAG.Aula> CargarAulas()
        {
            List<sadaAG.Aula> resultado = new List<sadaAG.Aula>();
            try
            {
                List<Usuario_Institucion> items = contexto.Usuario_Institucions
                    .Include(ui => ui.IdInstitucionNavigation.Aulas)
                        .ThenInclude(a => a.Aula_Recursos)
                            .ThenInclude(ar => ar.IdRecursoNavigation)
                    .Where(ui => ui.IdUsuario == idUsuario && ui.IdInstitucion == idInstitucion)
                    .ToList<Usuario_Institucion>();

                if (items.Count > 0)
                { 
                    Usuario_Institucion usuarioInstitucion = items.ToList<Usuario_Institucion>()[0];

                    
                    foreach (Datos.Aula aula in usuarioInstitucion.IdInstitucionNavigation.Aulas)
                    {
                        sadaAG.Aula aulaAG = new sadaAG.Aula()
                        {
                            Id = aula.Id,
                            Codigo = aula.Codigo,
                            Capacidad = aula.Capacidad,
                            IdsPrestaciones = new List<int>()
                        };

                        CargarRecursos(aulaAG, aula);

                        resultado.Add(aulaAG);
                    }
                }
            } catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
            }

            return resultado;
        }
        private void CargarRecursos(sadaAG.Aula aulaAG, Datos.Aula aula)
        {
            foreach (Datos.Aula_Recurso ar in aula.Aula_Recursos)
            {
                aulaAG.IdsPrestaciones.Add(ar.IdRecurso);
            }
        }
        private void CargarRecursos(sadaAG.Comision comisionAG, Datos.Comision comision)
        {
            foreach (Datos.Comision_Recurso cr in comision.Comision_Recursos)
            {
                comisionAG.IdsPrestaciones.Add(cr.IdRecurso);
            }
        }
        private List<sadaAG.Comision> CargarComisiones()
        {
            List<sadaAG.Comision> resultado = new List<sadaAG.Comision>();

            List<Usuario_Institucion> items = contexto.Usuario_Institucions
                .Include(ui => ui.IdInstitucionNavigation.Asignaturas)
                    .ThenInclude(asig => asig.Comisions)
                        .ThenInclude(c => c.Comision_Recursos)
                            .ThenInclude(cr => cr.IdRecursoNavigation)
                .Where(ui => ui.IdUsuario == idUsuario && ui.IdInstitucion == idInstitucion)
                .ToList<Usuario_Institucion>();

            if (items.Count > 0)
            {
                Usuario_Institucion usuarioInstitucion = items.ToList<Usuario_Institucion>()[0];

                foreach (Datos.Asignatura asignatura in usuarioInstitucion.IdInstitucionNavigation.Asignaturas)
                {
                    foreach (Datos.Comision comision in asignatura.Comisions)
                    {
                        sadaAG.Comision comisionAG = new sadaAG.Comision()
                        {
                            Id = comision.Id,
                            Codigo = comision.Codigo,
                            Capacidad = comision.Capacidad,
                            Horas = comision.Horas,
                            AsignaturaNombre = asignatura.Nombre,
                            IdsPrestaciones = new List<int>()
                    };

                        CargarRecursos(comisionAG, comision);

                        resultado.Add(comisionAG);
                    }
                }
            }

            return resultado;
        }
        private List<sadaAG.Recurso> CargarRecursos()
        {
            List<sadaAG.Recurso> resultado = new List<sadaAG.Recurso>();

            try { 
                var recursos = from r in contexto.Recursos
                                 select r;

                foreach (Datos.Recurso recurso in recursos)
                {
                    sadaAG.Recurso recursoAG = new sadaAG.Recurso()
                    {
                        Id = recurso.Id,
                        Nombre = recurso.Nombre,
                        Peso = recurso.Peso
                    };

                    resultado.Add(recursoAG);
                }

            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
            }
            return resultado;
        }

        public async Task<string> GuardarSolucion(SolucionAG solucionAG)
        {
            string json = JsonSerializer.Serialize(solucionAG);

            Solucion solucion = new Solucion()
            {
                FechaEjecucion = DateTime.Now,
                IdInstitucion = this.idInstitucion,
                Imprecision = solucionAG.Imprecision,
                JSON = json
            };

            contexto.Add(solucion);
            await contexto.SaveChangesAsync();

            solucionAG.Id = solucion.Id;

            return JsonSerializer.Serialize(solucionAG);
        }

        public void Dispose()
        {
            contexto.Dispose();
        }
    }
}
