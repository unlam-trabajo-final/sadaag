#### SADA - Definición del núcleo del sistema
Martín M. Machuca, Emmanuel Senatori, Mariana Martinez, Matías Martinez,
Leila Coronel\
Trabajo final\
Borrador del núcleo del sistema

### 0. Modelo de Clases
------
![PlantUML Model](https://www.plantuml.com/plantuml/svg/hLdDZjis43wlWNq70RqunZgB14KFmSA8Ow-J1Th4YDrSWXneYUlbXX9TadBWRFfGHOyzz907wYjqXfGeIg9afoa2n1Bdc-5m_gdzvw-_dojDfCvJ7iqPsKgIaYYXqOmJHTNvG7rasOx0QgJqWLFuNx8OqMtIdDwIdEk5oFHhaj9eAXdfXwtOPnez-QaBOo3cVIDuqaPDfHIVrYp-c56begUFdtP0Pe8B6Vq0PpkS3-uOd8wcEts8Kffkg8H3dW_81Wv6OXt5S6uLJVbMIAPJyP9cLBDOJ0kwYAwkRbhKoUHyqC4OeVGEsekSyOJAwE5yS3Pz6Jrh8u2m4u83wMUcT6XdGCma9R956Jxg45Y7goLOYrCkrF2ESG4yBDF8sh7PYkgP54g9FPN3k7m2_8rR3cvSdDLAMEHPp4IsO1ezDxojlQAiEk2um5zoJP1Xc1OFAAHSFCw-efp6Xb-LJoZ0BH-NC2SQyaKFYz-J6T2jG-_vEEECP9eb946YDEUZQbZux3JEconXUvRauaI-QyuqKwaORY0KZyET-0NcYpmCRveh_J9-xyoNCbHDmi1f22R6x8va6ULNg1m6w3JUwfnmzfb80t5QuwylFUV42Bf9RD2ZJ96mBUO0LhbiQtRPWOt1Q2N6ySz4oXG6ob4PhuGaoj7DsrInRKOIRxbd8vTaK7hSosHYZ21pE2S63b5NwydGL8drv29o3uM7Y_Vt7u1fZivWbklNR9bdMjHusonEbo9IRSK20wB-S-W7O0SY3rV0UM4CQyfIOI0Q0fL412ubLRg8v02q8a-isqqG6pEkQ8ekfgfbD2-bQigQGmKrD9GUjGpzMtG1ijmI-MPpJpMa6ffqBnYqiJCjodA1YyMpKRxHXgwj_9d8QihOY5CX1M7dKtL7vNmSb66QQLej1HWVUIQ-tWDyAhUgMX0yHzXDnk37qQCMO2d55eJsGHOiEq60CSLhaMuazJEkgetE6R_GUo6BVZADiK8uqf9sKNocLs0AA4UngFY4Z453SayXR7QSQf8WCH7vXbFaPbaEdd-BrWF3Le6nejiyIqAKWcKjIQRYN4e6g1PRc0fP3RDCsHcWN19VbwNOyBf1rcAsUcS2i3IRysoz-sBPgQzKumviXMJucKoCBzuH7BMQsGXqD54EvnKLliPWvnia01yu70HGUKjY6hNda0wlpuBJ210MH-OBD7nWRZ0c3uq33NjQrSiUD_6fQnWqeJZm82hKoUkXHrNLgxsoqcpZZbHsQjVjpig2xPMPCxysFlCh9PQLHgl6SpVNK3kXsHvcHvgPJbiOmde26uQH14N0zc5d3hy8litjQyWkF8OABTiHRHXSJ5_VqnXS9T_im7m9MgiC45jjNSFtOgcAaQfzmXHKxFu4Zrk-O2Ci7f_wwq6bwxiq0WwtIWAEkDuJdllxDxSfZrgNtlBHGpdLEyfR0eKYDODbELKwMbCS6AtsxbBXeLRZGCGj5qGNoTAZTNqdMm7Scf5QPQADs-9zJXLSGEdMrj6odxt_O2yv0Yuh8YMDMd7cns1pVh01UL7OiMZvS3saQp4Kq9TZOvaYhJZo3Es2Ey3PTFuwN2HRNe2wrboh2cMJyeABtw0S_cXdmo1hrCKxPwZCnZJ2Ls2LBKGzwPJXURQgPWz-RODJxxhBsKLK3j0XlxfWmVtBbw8Nd18x9uIEDv0qHkrs2rZYJD3iFjMnGKVtOWVexpG2_Wlqfp6djteygNMXFbYp7OMm4xynTG2y3XL0ZCQ9-9GXpYLZM589QnV-7hGhQySnjjU4SFaEK5rpv2aihSdo52P_rloMJT3xdHzrd1K3ts3E1sLV7bdHU2DfBPQNaHQXoWXFb6dd8N4-eINI9_fYVN8Pfix37L5_zUNnuxulPy1avaM4cQPJUtK38gnxLrwa-bdKd4sHtaeVLvKCsKiRhnGWnSkNW2UzRtvVnkFGjxHlPd5VC733ZjkTvJGUx4DKhkn0ZSQjKAbkL4P4Tw9r0GFfrGMj9nNYEYz_uwxagWgLSEba8wUA4B-UjUEZGNM1qLYlug9E05AhpzjayRdRa49PwyLkUkN34Cg4QA06dmQiJHWdSmLd2rGl4vf6M0WYSTlz9HiwwkNbacZvzSyC_ue0ukZNFyJb9H16em8r6fqFQ9P4AB8XkEc-Vdaho3LDjuS-WLMW_6TPactlhB0gJFu7OTury_lEM5qeUkGi-E4JnRtxTAhV1RzNMkZYs2Eph9rHWUuviLTO-YI-sIWgzys0UmwFUSh_1G00)


### 1. Introducción
---------------

El motor de procesamiento de la aplicación SADA (Sistema automático de
asignación de aulas) será la implementación especial de un Algoritmo
Genético (AG) utilizando programación orientada a objetos en lenguaje
C\#, patrones de diseño y el acceso a una base de datos MS-SQL Server
para la persistencia de los datos.

Debemos prestar especial atención en la codificación de los genes y la
función de fitness porque ellos son la clave del éxito o fracaso de todo
el proyecto.

### 2. Descripción del problema
---------------------------

Se necesita organizar un grupo de comisiones en un conjunto de aulas
disponibles. La asignación comisión-aula va a depender de las
necesidades o requerimientos de la comisión y la disponibilidad y
prestaciones del aula.

Las prestaciones serán codificadas en la base de datos y a cada aula se
le asignarán las que le correspondan. Como ejemplo de prestaciones
podríamos citar: wifi, PCs, toma corrientes, pizarrón, proyector, mesas
para trabajar en equipo, climatización, etc.

La capacidad es la cantidad estudiantes que soporta un aula.
Análogamente también representa la cantidad de estudiantes que compone
una comisión.

Las comisiones se definen también con un horario de cursada, este
horario debe coincidir con la disponibilidad horaria de un aula, de modo
que no podrán superponerse dos comisiones distintas en el mismo horario,
en la misma aula.

En definitiva, al programa se le presentará un grupo de aulas
configuradas con sus respectivas prestaciones y un conjunto de
comisiones que deben ser asignadas a aquellas. El programa deberá
encontrar alguna de las combinaciones óptimas para satisfacer los
requerimientos de la mayor cantidad de comisiones. Estas combinaciones,
mejor llamadas soluciones, pueden ser varias y, de ser posible, se le
presentarán al usuario las tres mejores.

  **Lunes**    **Aula 1**    **Aula 2**    **Aula 3**    **...**   **Aula N**
  ------------ ------------- ------------- ------------- --------- --------------
  **Hora 1**   Comisión 1    Comisión 2    Comisión 30   ...       Comisión 45
  **Hora 2**   Comisión 1    Comisión 34   Comisión 30   ...       Comisión 45
  **Hora 3**   Comisión 20   Comisión 34   Comisión 30   ...       Comisión 11
  **...**      ...           ...           ...           ...       ...
  **Hora M**   Comisión 3    Comisión 7    Comisión 14   ...       Comisión 100

  **Martes**   **Aula 1**    **Aula 2**    **Aula 3**    **...**   **Aula N**
  ------------ ------------- ------------- ------------- --------- --------------
  **Hora 1**   Comisión 50   Comisión 2    Comisión 35   ...       Comisión 45
  **Hora 2**   Comisión 41   Comisión 34   Comisión 30   ...       Comisión 45
  **Hora 3**   Comisión 41   Comisión 34   Comisión 30   ...       Comisión 33
  **...**      ...           ...           ...           ...       ...
  **Hora M**   Comisión 3    Comisión 7    Comisión 15   ...       Comisión 100

...
---

  **Sábado**   **Aula 1**    **Aula 2**   **Aula 3**    **...**   **Aula N**
  ------------ ------------- ------------ ------------- --------- -------------
  **Hora 1**   Comisión 50   Comisión 2   Comisión 35   ...       Comisión 45
  **...**      ...           ...          ...           ...       ...

*Figura 1 - Ejemplo de asignación de aulas*

### 3. Definición del Algoritmo Genético
------------------------------------

Los algoritmos genéticos tienen una estructura y funcionamiento general
con algunas variantes. Lo interesante es expresarlo de manera correcta
para que se adapte al dominio del problema planteado y resulte eficiente
en su ejecución.

### 3.1 Codificación

La codificación del problema es uno de los puntos más importantes, en
ella se basarán las estructuras de datos a utilizar y de estas dependerá
el consumo de memoria, ya que debemos tener en cuenta que cada individuo
estará representado por esta estructura de datos. Usaremos una gran
cantidad de individuos para garantizar una búsqueda exhaustiva y si sus
estructuras de datos no son óptimas, por ej. si representan más
información de la necesaria, entonces tendremos una baja performance en
la ejecución y un alto consumo de memoria.

En nuestro caso necesitamos representar la grilla horaria de todos los
días de la semana, tal como expresa la Figura 1.

Cada casillero de esta grilla contendría una instancia de espacio y
tiempo para ser ocupado por una comisión, en caso de coincidencia de
requerimientos.

### 3.1.1 Cromosomas y genes

Proponemos invertir la grilla anterior, de esta forma podremos
visualizar que cada aula será representada por un gen y todas las aulas
juntas representarán un cromosoma o individuo.

           **Hora 1**   **Hora 2**   **Hora 3**   **Hora 4**   **Hora 5**   **Hora 6**   **Hora 7**   **Hora 8**
  -------- ------------ ------------ ------------ ------------ ------------ ------------ ------------ ------------
  Aula 1   1            1            1            1                         8            8            
  Aula 2   2            2            2            7            7            9            9            9
  Aula 3   3            4            4            5            5            5            6            6
  Aula 4   56           56           60           60           10           10                        
  Aula 5                                                                                              

*Figura 2 -- Representación invertida*

Aquí nos enfrentamos al problema de la codificación del cromosoma: su
representación como vector unidimensional sería demasiado extensa al
tener que cubrir la totalidad de las celdas de la matriz.

Por tanto, se propone una modificación experimental a la estructura
clásica del gen en AG, convirtiendo a cada gen en un cromosoma más
pequeño, es decir en un vector de valores enteros.

Esto también impacta en el funcionamiento de los operadores genéticos,
fundamentalmente para el cruce (crossover), que deberán tener en cuenta
esta nueva complejidad para intervenir en el interior de cada gen con el
fin de aportar diversidad en la búsqueda de soluciones.

\[(1,1,1,1,0,8,8,0),(2,2,2,7,7,9,9,9),(3,4,4,5,5,5,6,6),(56,56,60,60,10,10,0,0),(0,0,0,0,0,0,0,0)\]

*Figura 3 -- Cada gen es representado por un vector de valores enteros.*

En la Figura 3 se puede observar la codificación de un individuo o
solución. Cada grupo de valores encerrados entre paréntesis
representaría la asignación de comisiones para un aula. En este ejemplo
acotamos hasta 8 horas pero en la implementación real deberíamos agregar
el total de las horas semanales, es decir todos los días de la semana
desagregados por horas.

### 3.2 Operadores genéticos

Los operadores genéticos son los que posibilitan la combinación de genes
de individuos padres para generar una nueva población más eficiente en
términos de la resolución del problema.

En nuestra implementación diseñaremos los distintos operadores genéticos
utilizando patrones de diseño, Strategy por ejemplo, para poder
experimentar con distintas versiones de cada operador genético. De esta
forma podremos encontrar la mejor configuración del algoritmo genético
para nuestro problema y a la vez generaremos un código fuente legible,
ordenado y extensible.

A continuación describiremos algunos tipos de operadores genéticos que,
a priori, consideramos ideales para nuestra implementación. No obstante,
es probable que implementemos otras variantes de operadores genéticos.

### 3.2.1 Selección por torneo

Existen distintos métodos de selección para la reproducción de los
individuos: selección por ruleta, selección por torneo, etc.

En nuestro caso utilizaremos la selección por torneo. Este método
selecciona azar dos individuos de la generación actual para competir por
posibilidad de reproducirse. El más apto de los dos es seleccionado y el
perdedor es descartado.

Consideramos que este es un buen método para mantener la diversidad
genética. Si sólo se seleccionaran los individuos más aptos de cada
generación, el algoritmo convergería tempranamente en una solución
optima local, perdiendo la posibilidad de explorar el resto del campo de
búsqueda. Al hacer un torneo entre dos individuos al azar podría ser que
se seleccionen dos buenos individuos: sólo uno se reproduciría y se
perdería el otro. Asumimos este riesgo en pos de mantener la diversidad
genética, pues también los menos aptos de una generación tendrán
oportunidad de tener descendencia y su información genética, que en una
generación temprana no representa una buena solución, quizás en una
generación posterior podría aportar información genética vital para
hallar la solución óptima.

### 3.2.2 Crossover (Cruce)

Una vez seleccionados los individuos para la reproducción se procederá a
formar parejas para la reproducción. La forma de reproducción general es
tomar trozos de información de ambos padres y generar dos nuevos
individuos. Hay varias formas de realizar esto, las más clásicas son el
cruce de 1 punto, el cruce de 2 puntos y el cruce uniforme.

### 3.2.2.1 Crossover recursivo

Este es nuestro aporte al método crossover. Debido a la naturaleza
multidimensional de nuestra codificación deberemos tener en cuenta esta
estructura de datos compleja.

Si utilizáramos un cruce clásico estaríamos atados hasta el final a la
diversidad de combinaciones creadas al azar en la generación 0. Y sólo
las cambiaríamos de aula, pero no variarían las combinaciones de aulas.
Una estructura como la primera del ejemplo de la Figura 3
*(1,1,1,1,0,8,8,0)* permanecería intacta hasta el final y sólo
presenciaríamos un intercambio de lugares entre estructuras fijas.

Para remediar este problema y aportar más diversidad en la búsqueda
deberemos intervenir también en el interior de estas estructuras.

Entendemos a modo experimental que se podría hacer un crossover de dos
puntos a nivel del cromosoma. Y al nivel de gen podríamos aplicar un
crossover de un solo punto.

También la operación de crossover podría ser implementada una sola vez y
llamarla de forma recursiva, tanto para el cromosoma como para el gen.

### 3.2.3 Elitismo

Este método se aplica para cuidar la genética de los mejores individuos.
Aplicaremos el elitismo dejando pasar a la siguiente generación un
porcentaje bajo de las mejores soluciones. Esto se hace para evitar que
se pierdan las buenas soluciones entre generaciones, algo muy probable
durante la combinación genética.

### 3.2.4 Mutación

Imitando a la naturaleza se aplicará un porcentaje mínimo (no más del
2%) de mutación en algún gen de los individuos seleccionados para tal
fin.

En la naturaleza aparece la mutación como un error en la copia de
información genética. Si bien en la mayoría de los casos es perjudicial
para el individuo que la sufre, en otros casos la mutación es algo
positivo que fortalece la especie.

Con la mutación reforzamos un poco más la diversidad en la exploración
del campo de búsqueda.

### 3.3 Método de evaluación: La función Fitness

Es la función clave en la implementación de nuestro algoritmo genético,
debe representar la lógica de nuestro dominio.

Cada individuo tendrá asignado un valor Fitness que representará el
nivel de error de la solución, por lo tanto necesitamos que ese valor
tienda a 0. Ni bien un individuo tenga fitness 0, querrá decir que
obtuvimos una solución óptima.

De este modo, debemos enumerar una serie de reglas para evaluar a los
individuos:

-   Las asignaciones de comisión y aula deberán ser coincidentes
    respecto a sus requerimientos y prestaciones.

    -   Sería conveniente asignar un puntaje a cada prestación,
        asignándoles así niveles de importancia. Por ej.: Acondicionador
        de aire es una característica preferible pero no indispensable.
        Por otro lado, la capacidad suficiente del aula es una
        prestación indispensable.

-   No deberá existir superposición de horario y aulas entre comisiones.

-   No deberá existir superposición horaria para una misma comisión en
    varias.

-   Es preferible el agrupamiento horario.

### 3.4 Pseudocódigo del algoritmo principal


Una vez definidas todas las partes anteriores es momento de definir de
forma general el funcionamiento del algoritmo principal. Este es
sencillo consta de unas cuantas instrucciones y un ciclo de repetición
que se ejecutará hasta que se cumpla alguna de las condiciones de
terminación. En nuestro caso nuestra condición de terminación será
cuando el algoritmo converja, es decir cuando encuentre una solución
óptima o aceptable, o cuando haya alcanzado una cantidad tope de
generaciones.

A continuación, mostramos el pseudocódigo:

    Comienzo:

      Inicializar población.

      Evaluar población inicial, calcular Fitness.

      Generación = 0.

      Hacer Mientras (Fitness>0 Y Generación<TOPE Y Diversidad Genética>0.005)

        Ejecutar Selección.

        Ejecutar Elitismo.

        Ejecutar Crossover (nuevos individuos).

        Ejecutar Mutación.

        Calcular Fitness.

        Medir Diversidad genética.

        Eliminar los individuos menos aptos.

        Incrementar Generación.

      Fin Mientras.

      Devolver las tres mejores soluciones (los individuos más aptos).

    Fin.

### 4. Implementación y pruebas
---------------------------

Se implementó el algoritmo como una librería escrita en C\#, .NET Core
3.8. Se utilizó programación orientada a objetos y se diseñó siguiendo
patrones de diseño clásicos, con la idea de obtener un modelo de objetos
claro y reutilizable

Los datos de entrada (Comisiones, Aulas, cantidad de horas semanales,
Prestaciones) son proporcionados en un archivo de formato JSON, que es
deserializado y convertido a colecciones de objetos contenidos en un
Dataset visible por todos los componentes del sistema.

El sistema encuentra efectivamente soluciones óptimas o cercanas en un
tiempo promedio de 2 minutos.

Debido al importante consumo de tiempo de procesamiento, el algoritmo
emite eventos con información acerca del progreso de la evolución.

La información provista en cada evento es:

-   Generación (ciclo de iteración)

-   Mejor solución de la iteración actual

-   Peor solución en la iteración actual

-   Mejor solución encontrada en todas las iteraciones

-   Porcentaje de diversidad genética de la población actual (diversidad = distintos/población)

-   Porcentaje de completitud

-   Tiempo transcurrido

-   Tiempo restante

La mejor configuración probada fue con poblaciones de 15000 individuos y
100 iteraciones (generaciones) en una notebook con 8GB de memoria RAM y
un procesador Intel Core i5-6200U CPU @ 2.30GHz 2.40GHz, tomando unos 2
minutos de promedio alcanzar la solución óptima.

Para garantizar la diversidad genética y evitar una rápida convergencia
del algoritmo, se le aplicó un porcentaje de mutación del 1%.

### 5. Conclusiones
---------------

Luego de realizar este trabajo hemos verificado una vez más, y de manera
empírica, que los algoritmos genéticos son una herramienta muy útil para
la búsqueda y optimización de soluciones.

Como ejemplo, podemos proponer un caso realista. Supongamos que tenemos
que configurar las asignaciones de horarios para:

-   7 Aulas

-   10 Comisiones

-   12 horas semanales

-   3 Prestaciones

Si utilizáramos el método de búsqueda secuencial (fuerza bruta) para
encontrar una solución óptima, en el peor de los casos, deberíamos
revisar una cantidad ingente de combinaciones:

7\^10\^12\^3 = 1.71907333981E304 individuos a evaluar

Con nuestro algoritmo genético de 100 generaciones (iteraciones) y una
población de 15000 individuos:

15000 \* 100 = 1500000 individuos a evaluar

Tomando el peor de los casos, nuestro algoritmo tardaría alrededor de 4
minutos. Prorrateando ese tiempo entre todos los individuos generados,
la creación y evaluación de cada uno tomaría unos 0.16 milisegundos.

Y si trasladamos este valor a la búsqueda secuencial podemos inferir el
tiempo que llevaría recorrer todo el campo de búsqueda:

1.71907333981E304 \* 0.16ms = 2.7505173437E303ms

2.7505173437E303ms/1000/3600/24/365 = **8.72183328164E292 Años!!!!**

Cabe aclarar que habrá más de una solución en todo el campo de búsqueda,
y que con el método de búsqueda secuencial no necesariamente se debería
llegar hasta el final. Pero vale el cálculo como panorama de la magnitud
del problema.

Otra ventaja con la que cuenta el método del algoritmo genético es la
posibilidad de medir las distancias hipotéticas que hay entre las
distintas soluciones, algo que la búsqueda secuencial no nos podría dar
en este caso.

Como último comentario debemos mencionar la búsqueda aleatoria. Por
tratarse de la probabilidad de 1 sobre el total del campo de búsqueda,
P(x) = 1/1.71907333981E304, por cada intento, podemos inferir que esta
búsqueda podría tornarse infinita.

Tanto la búsqueda secuencial como la aleatoria son métodos inviables y
computacionalmente imposibles de llevar a cabo.

Por ello, los métodos evolutivos siguen siendo de gran aporte para este
tipo de problemas.
